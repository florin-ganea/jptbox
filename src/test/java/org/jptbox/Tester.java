/*
 * Copyright (c) 2013, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox;

//import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author florin.ganea
 */
public class Tester {
    
    private static final Logger LOG = Logger.getLogger(Tester.class.getName());
    
    private static class Element {
        final int x;
        int y;
        
        Element() {
            this.x = 3;
            this.y = 4;
        }
    }
    
    private class ProducerThread extends IterativeThread {

        ProducerThread(int maxIterations) {
            super("Producer", maxIterations);
        }

        @Override
        public void runIteration() {
            element = new Element();
        }
        
    }
    
    private class ConsumerThread extends IterativeThread {
        
        ConsumerThread() {
            super("Consumer");
        }
        
        @Override
        @SuppressWarnings("UseOfSystemOutOrSystemErr")
        public void runIteration() {
            Element el = element;
            if (el != null) {
                int y = el.y;
                int x = el.x;
                if (x != 3) {
                    System.err.println("wrong x, seen: " + x);
                }
                if (y != 4) {
                    System.err.println("wrong y, seen: " + y);
                }
            }
        }
        
    }
    
    //final ConcurrentLinkedQueue<Element> queue;
    volatile Element element;
    
    {
        //queue = new ConcurrentLinkedQueue<>();
        element = null;
    }
    
    /**
     *
     * @param args
     */
    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    public static void main(String[] args) {
        Tester t = new Tester();
        IterativeThread pt = t.new ProducerThread(0);
        IterativeThread ct = t.new ConsumerThread();
        ct.start();
        System.out.println(ct.getName() + " started");
        pt.start();
        System.out.println(pt.getName() + " started");
        
        // timed run
        try {
            Thread.sleep(1000L * 300);
        } catch (InterruptedException ex) {
            LOG.log(Level.SEVERE, "interrupted", ex);
        }
        
        pt.shutdown();
        pt.waitTerminated(0);
        System.out.println(pt.getName() + " terminated");
        //System.out.println("queue size: " + t.queue.size());
        ct.shutdown();
        ct.waitTerminated(0);
        System.out.println(ct.getName() + " terminated");
        
        synchronized (ct) {
            try {
                ct.wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
}
