/*
 * Copyright (c) 2013-2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox;

import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Unit tests for {@link IterativeThread}.
 * 
 * @author Florin Ganea
 * @version 1.0
 */
public class IterativeThreadTest {
    
    private static final Logger LOG = Logger.getLogger(IterativeThreadTest.class.getPackage().getName());

    /**
     *
     */
    public IterativeThreadTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of execute method, of class IterativeThread.
     */
    @Test
    public void testExecute() {
        LOG.finest("execute");
        IterativeThreadImpl instance = new IterativeThreadImpl(10);
        instance.execute();
        // thread not started (just execute method called)
        assertFalse(instance.isAlive());
        // check the number of iterations; should be 0 since the execute is
        // not called in running status
        assertEquals(0, instance.iterations);
        // use run method that indirectly calls execute
        instance.run();
        // thread not started (just execute method called)
        assertFalse(instance.isAlive());
        // check the number of iterations
        assertEquals(0, instance.iterations);
        // start the thread; new object is required after calling run...
        instance = new IterativeThreadImpl(10);
        instance.start();
        instance.waitTerminated(0);
        // check the number of iterations
        assertEquals(10, instance.iterations);
    }

    /**
     * Test of runIteration method, of class IterativeThread.
     */
    @Test
    public void testRunIteration() {
        LOG.finest("runIteration");
        IterativeThreadImpl instance = new IterativeThreadImpl(10);
        // calling iteration method does not obey to max iterations limit
        for (int i = 0; i < 100; i++) {
            assertEquals(i, instance.iterations);
            instance.runIteration();
        }        
    }

    /**
     * Test of shutdown method, of class IterativeThread.
     */
    @Test
    public void testShutdown() {
        System.out.println("shutdown");
        IterativeThreadImpl instance = new IterativeThreadImpl(0);
        // not started
        assertFalse(instance.isRunning());
        instance.start();
        instance.waitRunning();
        // started
        assertTrue(instance.isRunning());
        synchronized(instance) {
            try {
                instance.wait(100L);
            } catch (InterruptedException ex) {
                fail("interrupted sleep");
            }
        }
        instance.shutdown();
        instance.waitTerminated(0);
        // stopped
        assertFalse(instance.isRunning());
        // stopped before max iterations
        assertTrue(instance.iterations < Integer.MAX_VALUE);
        System.out.println("iterations=" + instance.iterations);
    }
    
    /**
     * Test of shutdown method, of class IterativeThread.
     */
    @Test
    public void testShutdown_2() {
        System.out.println("shutdown_2");
        IterativeThreadImpl instance = new IterativeThreadImpl(Integer.MAX_VALUE);
        // not started
        assertFalse(instance.isRunning());
        instance.start();
        instance.waitRunning();
        // started
        assertTrue(instance.isRunning());
        instance.shutdown();
        instance.waitTerminated(0);
        // stopped
        assertFalse(instance.isRunning());
        // stopped before max iterations
        assertTrue(instance.iterations < Integer.MAX_VALUE);
        System.out.println("iterations=" + instance.iterations);
    }

    private static class IterativeThreadImpl extends IterativeThread {

        public int iterations;
        
        {
            this.iterations = 0;
        }
        
        private IterativeThreadImpl(int max) {
            super(max);
        }

        @Override
        public void runIteration() {
            this.iterations++;
        }

    }

}