/*
 * Copyright (c) 2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jptbox.logging;

import java.io.IOException;
import java.util.logging.Level;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Florin Ganea
 */
public class DebugLoggerTest {
    
    /**
     *
     */
    public DebugLoggerTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class DebugLogger.
     */
    @Test
    public void testGetInstance_0args() {
        System.out.println("getInstance");
        DebugLogger expResult = DebugLogger.getInstance();
        DebugLogger result = DebugLogger.getInstance();
        assertEquals(expResult, result);
    }

    /**
     * Test of getInstance method, of class DebugLogger.
     */
    @Test
    public void testGetInstance_String() {
        System.out.println("getInstance");
        String loggerName = "test";
        DebugLogger expResult = DebugLogger.getInstance(loggerName);
        DebugLogger result = DebugLogger.getInstance(loggerName);
        assertEquals(expResult, result);
    }

    /**
     * Test of logf method, of class DebugLogger.
     */
    @Test
    public void testLogf() {
        System.out.println("logf");
        DebugLogger instance = DebugLogger.getInstance();
        instance.logf(Level.INFO, "The %s is %s.", "man", "mad");
        instance.logf(Level.WARNING, "The %s is %s too.", "man", "mad");
        instance.log(Level.SEVERE, "ups", new IOException("bad io"));
    }
    
    /**
     * Test of logf method, of class DebugLogger.
     */
    @Test
    public void testLogf2() {
        System.out.println("logf2");
        DebugLogger instance = DebugLogger.getInstance();
        
        StringBuilder sb = null;
        for (StackTraceElement ste : new Exception().getStackTrace()) {
            if (sb == null) {
                sb = new StringBuilder(1024);
            } else {
                sb.append(System.lineSeparator());
            }
            sb.append(ste);
        }
        
        instance.logf(Level.FINEST, "current thread stack via exception:%n%s", sb);
    }

}
