/*
 * Copyright (c) 2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jptbox.codec;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import org.jptbox.logging.DebugLogger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Florin Ganea
 */
public class Base64Test {
    
    /**
     *
     */
    public Base64Test() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of encode method, of class Base64.
     */
    @Test
    public void testEncode_byteArr() {
        byte[] data = new byte[0];
        Base64 instance = new Base64();
        char[] expResult = new char[0];
        char[] result = instance.encode(data);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of decode method, of class Base64.
     */
    @Test
    public void testDecode() {
        char[] encoding = new char[0];
        Base64 instance = new Base64();
        byte[] expResult = new byte[0];
        byte[] result = instance.decode(encoding);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of encode method, of class Base64.
     */
    @Test
    public void testEncode_long() {
        long value = 0L;
        Base64 instance = new Base64();
        String expResult = "AAAAAAAAAAA=";
        String result = instance.encode(value);
        assertEquals(expResult, result);
    }

    /**
     * Test of decodeLong method, of class Base64.
     */
    @Test
    public void testDecodeLong() {
        String encoding = "AAAAAAAAAAA=";
        Base64 instance = new Base64();
        long expResult = 0L;
        long result = instance.decodeLong(encoding);
        assertEquals(expResult, result);
    }
    
    /**
     *
     */
    @Test
    public void testRandomLongTranscoding() {
        Random rnd = new Random();
        Base64 codec = new Base64();
        
        for (int i = 0; i < 10000000; i++) {
            long lval = rnd.nextLong();
            
            String senc = codec.encode(lval);
            
            assertEquals(12, senc.length());
            assertTrue(senc.indexOf('=') == 11);
            assertTrue(senc.lastIndexOf('=') == 11);
            
            long dval = codec.decodeLong(senc);
            
            assertEquals(lval, dval);
        }
    }
    
    /**
     *
     */
    @Test
    public void testRandomLongTranscodingExt() {
        class ByteUtils {
            private final ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);    

            byte[] longToBytes(long x) {
                buffer.clear();
                buffer.putLong(0, x);
                return buffer.array();
            }

            long bytesToLong(byte[] bytes) {
                buffer.clear();
                buffer.put(bytes, 0, bytes.length);
                buffer.flip(); //need flip 
                return buffer.getLong();
            }
        }
        
        Random rnd = new Random();
        Base64 codec = new Base64();
        ByteUtils bu = new ByteUtils();
        byte[] lb = new byte[Long.BYTES];
        
        for (int i = 0; i < 10000000; i++) {
            rnd.nextBytes(lb);
            long lval = bu.bytesToLong(lb);

            String senc = codec.encode(lval);
            
            assertEquals(12, senc.length());
            assertTrue(senc.indexOf('=') == 11);
            assertTrue(senc.lastIndexOf('=') == 11);
            
            long dval = codec.decodeLong(senc);
            
            // check the sanity of the number generator
            byte[] dba = codec.decode(senc.toCharArray());            
            for (int j = 0; j < Long.BYTES; j++) {
                assertEquals(lb[j], dba[j]);
            }
            
            assertEquals(lval, dval);
        }
    }

    /**
     * random failure: -1388254134274713982
     */
    @Test
    public void testRandomLongTranscoding2() {
        Base64 codec = new Base64();
        long lval = -1388254134274713982L;
        String senc = codec.encode(lval);
        long dval = codec.decodeLong(senc);
        assertEquals(lval, dval);
    }

    /**
     *
     */
    @Test
    public void testRandomLongTranscoding3() {
        Base64 codec = new Base64();
        long lval = -1;
        String senc = codec.encode(lval);
        assertEquals("//////////8=", senc);
        long dval = codec.decodeLong(senc);
        assertEquals(lval, dval);
    }

    /**
     *
     */
    @Test
    public void testRandomLongTranscodingMinValue() {
        Base64 codec = new Base64();
        long lval = Long.MIN_VALUE;
        String senc = codec.encode(lval);
        assertEquals("gAAAAAAAAAA=", senc);
        long dval = codec.decodeLong(senc);
        assertEquals(lval, dval);
    }

    /**
     *
     */
    @Test
    public void testRandomLongTranscodingMaxValue() {
        Base64 codec = new Base64();
        long lval = Long.MAX_VALUE;
        String senc = codec.encode(lval);
        assertEquals("f/////////8=", senc);
        long dval = codec.decodeLong(senc);
        assertEquals(lval, dval);
    }

    /**
     * Test of encode method, of class Base64.
     */
    @Test
    public void testFastEncode_long() {
        long value = 0L;
        Base64 instance = new Base64();
        String expResult = "AAAAAAAAAAA=";
        String result = instance.fastEncode(value);
        assertEquals(expResult, result);
    }

    /**
     * Test of decodeLong method, of class Base64.
     */
    @Test
    public void testFastDecodeLong() {
        String encoding = "AAAAAAAAAAA=";
        Base64 instance = new Base64();
        long expResult = 0L;
        long result = instance.fastDecodeLong(encoding);
        assertEquals(expResult, result);
    }
    
    /**
     *
     */
    @Test
    public void testRandomLongFastTranscoding() {
        Random rnd = new Random();
        Base64 codec = new Base64();
        
        for (int i = 0; i < 3000000; i++) {
            long lval = rnd.nextLong();
            
            String senc = codec.fastEncode(lval);
            
            assertEquals(12, senc.length());
            assertTrue(senc.indexOf('=') == 11);
            assertTrue(senc.lastIndexOf('=') == 11);
            
            long dval = codec.fastDecodeLong(senc);
            
            assertEquals(lval, dval);
        }
    }
    
    /**
     * random failure: -1388254134274713982
     */
        @Test
    public void testRandomLongFastTranscoding2() {
        Base64 codec = new Base64();
        long lval = -1388254134274713982L;
        String senc = codec.fastEncode(lval);
        long dval = codec.fastDecodeLong(senc);
        assertEquals(lval, dval);
    }

    /**
     *
     */
    @Test
    public void testRandomLongFastTranscoding3() {
        Base64 codec = new Base64();
        long lval = -1;
        String senc = codec.fastEncode(lval);
        assertEquals("//////////8=", senc);
        long dval = codec.fastDecodeLong(senc);
        assertEquals(lval, dval);
    }

    /**
     *
     */
    @Test
    public void testRandomLongFastTranscodingMinValue() {
        Base64 codec = new Base64();
        long lval = Long.MIN_VALUE;
        String senc = codec.fastEncode(lval);
        assertEquals("gAAAAAAAAAA=", senc);
        long dval = codec.fastDecodeLong(senc);
        assertEquals(lval, dval);
    }

    /**
     *
     */
    @Test
    public void testRandomLongFastTranscodingMaxValue() {
        Base64 codec = new Base64();
        long lval = Long.MAX_VALUE;
        String senc = codec.fastEncode(lval);
        assertEquals("f/////////8=", senc);
        long dval = codec.fastDecodeLong(senc);
        assertEquals(lval, dval);
    }

    /**
     *
     */
    @Test
    public void testRandomTranscoding() {
        Random rnd = new Random();
        Base64 codec = new Base64();
        
        for (int i = 0; i < 100000; i++) {
            // random byte array
            int dataLen = rnd.nextInt(100);
            byte[] data = new byte[dataLen];
            rnd.nextBytes(data);
            
            // encode
            char[] enc = codec.encode(data);
            
            // check for well formed encoding
            assertTrue(enc.length % 4 == 0);
            for (int k = 0; k < enc.length; k++) {
                if (enc[k] == Base64.base64_pad) {
                    // padding is at most 2 char at the end
                    assertTrue(k >= enc.length - 2);
                    // padding is not followed by non-padding
                    assertTrue(k == enc.length - 1 || enc[k] == enc[k + 1]);
                    // padding is not used when data is multiple of 24-bit (3 bytes)
                    assertTrue(dataLen % 3 != 0);
                    // last 2 characters are padding for 8-bit (1 byte) remainder
                    if (dataLen % 3 == 1) {
                        assertTrue(k == enc.length - 1 || k == enc.length - 2);
                    }
                    // only last character is padding for 16-bit (2 bytes) remainder
                    if (dataLen % 3 == 2) {
                        assertTrue(k == enc.length - 1);
                    }
                } else {
                    // non-padding has to be valid character and 
                    // has a corresponding value between 0 and 63
                    assertTrue(Base64.base64_reverse[enc[k]] >= 0);
                    assertTrue(Base64.base64_reverse[enc[k]] <= 63);
                }
            }
            
            // decode back
            byte[] ddata = codec.decode(enc);
            
            // compare encoding with decoding
            assertArrayEquals(data, ddata);
        }
    }
    
    /**
     *
     */
    @Test
    public void testConsistency() {
        for (int i = 0; i < Base64.base64_alphabet.length; i++) {
            assertEquals(i, Base64.base64_reverse[Base64.base64_alphabet[i]]);
        }
        for (int i = 0; i < Base64.base64_reverse.length; i++) {
            // either valid index or -1 if not found
            int k = Base64.base64_alphabet.length - 1;
            while (k >= 0 && Base64.base64_alphabet[k] != i) {
                k--;
            }
            assertEquals(Base64.base64_reverse[i], k);
        }
    }

}
