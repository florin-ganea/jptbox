/*
 * Copyright (c) 2013, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox;

import static org.junit.Assert.*;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Unit tests for {@link BasicThread}.
 * 
 * @author Florin Ganea
 * @version 1.0
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class BasicThreadTest {
    
    private static final Logger LOG = Logger.getLogger(BasicThreadTest.class.getPackage().getName());
    
    /**
     *
     */
    public BasicThreadTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of nextThreadIndex method, of class BasicThread.
     */
    @Test
    public void testNextThreadIndex() {
        System.out.println("nextThreadIndex");
        
        // basic
        int initialIndex = IterativeThread.nextThreadIndex();
        int result = IterativeThread.nextThreadIndex();
        
        assertEquals(initialIndex + 1, result);
        
        // iterations
        initialIndex = result;
        for (int i = 1; i <= 100; i++) {
            result = IterativeThread.nextThreadIndex();
            assertEquals(initialIndex + i, result);
        }
    }

    /**
     * Test of nextThreadIndex method, of class BasicThread.
     */
    // -- disabled due long execution time -- @Test
    public void testNextThreadIndexMax() {
        System.out.println("nextThreadIndex -- max");
        
        // basic
        int initialIndex;
        int result = IterativeThread.nextThreadIndex();
        
        // max index
        initialIndex = result;
        boolean expectZero = false;
        while (!expectZero) {
            result = IterativeThread.nextThreadIndex();
            assertEquals(initialIndex + 1, result);
            initialIndex = result;
            expectZero = (result == Integer.MAX_VALUE);
            if (expectZero) {
                System.out.println("*** expectZero");
            }
        }
        result = IterativeThread.nextThreadIndex();
        assertEquals(0, result);        
    }

    /**
     * Test of run method, of class BasicThread.
     */
    @Test
    @SuppressWarnings("CallToThreadRun")
    public void testRun() {
        System.out.println("run");
        BasicThreadImpl instance = new BasicThreadImpl();
        instance.run();
        // thread not started (just run method called)
        assertFalse(instance.isAlive());
        assertFalse(instance.isRunning());
        assertTrue(instance.initialized);
        assertTrue(instance.executed);
        assertTrue(instance.terminated);
    }

    /**
     * Test of initialize method, of class BasicThread.
     */
    @Test
    public void testInitialize() {
        System.out.println("initialize");
        BasicThreadImpl instance = new BasicThreadImpl();
        instance.initialize();
        assertTrue(instance.initialized);
    }

    /**
     * Test of terminate method, of class BasicThread.
     */
    @Test
    public void testTerminate() {
        System.out.println("terminate");
        BasicThreadImpl instance = new BasicThreadImpl();
        instance.terminate();
        assertTrue(instance.terminated);
    }

    /**
     * Test of execute method, of class BasicThread.
     */
    @Test
    public void testExecute() {
        System.out.println("execute");
        BasicThreadImpl instance = new BasicThreadImpl();
        instance.execute();
        assertTrue(instance.executed);
    }

    /**
     * Test of waitRunning method, of class BasicThread.
     */
    @Test
    public void testWaitRunning() {
        System.out.println("waitRunning");
        BasicThread instance = new BasicThreadImpl();
        instance.start();
        instance.waitRunning();
        assertTrue(instance.isRunning());
        instance.shutdown();
        instance.waitTerminated(0);
        assertFalse(instance.isRunning());        
    }

    /**
     * Test of waitTerminated method, of class BasicThread.
     */
    @Test
    public void testWaitTerminated() {
        System.out.println("waitTerminated");
        BasicThread instance = new BasicThreadImpl();
        instance.start();
        instance.waitTerminated(0);
        assertFalse(instance.isRunning());
    }

    /**
     * Test of isRunning method, of class BasicThread.
     */
    @Test
    public void testIsRunning() {
        System.out.println("isRunning");
        BasicThread instance = new BasicThreadImpl();
        // not started
        assertFalse(instance.isRunning());        
        // start
        instance.start();
        instance.waitRunning();
        // started and running
        assertTrue(instance.isRunning());
        // wait to finish
        instance.waitTerminated(0);
        // terminated
        assertFalse(instance.isRunning());
    }

    /**
     * Test of shutdown method, of class BasicThread.
     */
    @Test
    public void testShutdown() {
        System.out.println("shutdown");
        BasicThreadImpl instance = new BasicThreadImpl();
        // not started
        assertFalse(instance.isRunning());
        instance.start();
        instance.waitRunning();
        // started
        assertTrue(instance.isRunning());
        instance.shutdown();
        instance.waitTerminated(0);
        // stopped
        assertFalse(instance.isRunning());
        // stopped before execution termination
        assertFalse(instance.executed);
    }

    private static class BasicThreadImpl extends BasicThread {

        public volatile boolean initialized;
        public volatile boolean terminated;
        public volatile boolean executed;
        
        {
            this.initialized = false;
            this.terminated = false;
            this.executed = false;
        }
        
        @Override
        public void initialize() {
            this.initialized = true;
        }

        @Override
        public void terminate() {
            this.terminated = true;
        }

        @Override
        public void execute() {
            try {
                Thread.sleep(2000);
                this.executed = true;
            } catch (InterruptedException ex) {
                LOG.log(Level.SEVERE, "interrupted", ex);
            }
        }
    }
}