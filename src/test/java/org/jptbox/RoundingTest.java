/*
 * Copyright (c) 2013-2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit tests for {@link Rounding} class.
 * 
 * @author Florin Ganea
 */
public class RoundingTest {
    
    /**
     *
     */
    public RoundingTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of top method, of class Rounding.
     */
    @Test
    public void testTop() {
        // System.out.println("top");
        Rounding r = new Rounding(1);
        assertEquals(0, r.top(0));
        for (long i = 1; i < 100; i++) {
            assertEquals(i, r.top(i));
        }
        r = new Rounding(11);
        assertEquals(0, r.top(0));
        assertEquals(11, r.top(1));
        assertEquals(11, r.top(11));
        assertEquals(22, r.top(12));
        
    }

    /**
     * Test of bottom method, of class Rounding.
     */
    @Test
    public void testBottom() {
        // System.out.println("bottom");
        Rounding r = new Rounding(1);
        assertEquals(0, r.bottom(0));
        for (long i = 1; i < 100; i++) {
            assertEquals(i, r.bottom(i));
        }
        r = new Rounding(11);
        assertEquals(0, r.bottom(0));
        assertEquals(0, r.bottom(1));
        assertEquals(11, r.bottom(11));
        assertEquals(11, r.bottom(12));
    }

}
