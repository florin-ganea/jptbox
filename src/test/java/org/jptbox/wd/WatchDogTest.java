/*
 * Copyright (c) 2013-2014, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.wd;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Test cases for the watchdog implementation.
 *
 * @author Florin Ganea
 * @version 1.0
 */
public class WatchDogTest {

    static final Logger LOGGER = Logger.getLogger(WatchDogTest.class.getPackage().getName());
    
    private static final ThreadMXBean threads = ManagementFactory.getThreadMXBean();
    private int startThreadCount;
    
    static final AtomicInteger pings = new AtomicInteger();
    static final AtomicInteger fails = new AtomicInteger();
    
    static final int ITERATIONS = 10;

    /**
     *
     */
    public WatchDogTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.startThreadCount = threads.getThreadCount();
        LOGGER.log(Level.INFO, "setup; number of running threads {0}", this.startThreadCount);
        pings.set(0);
        fails.set(0);
    }

    /**
     *
     * @throws Exception
     */
    @After
    public void tearDown()
            throws Exception {
        // WatchDog.shutdown();
        int thdiff = threads.getThreadCount() - this.startThreadCount;
        if (thdiff > 0) {
            LOGGER.log(Level.SEVERE, "hanging threads {0}", thdiff);
            long[] ths = threads.getAllThreadIds();
            for (int i = 0; i < ths.length; i++) {
                LOGGER.log(Level.SEVERE, "hanging thread {0}", threads.getThreadInfo(ths[i]));
            }
        }
        LOGGER.log(Level.INFO, "tear down; number of running threads {0}, diff {1}",
                new Object[]{threads.getThreadCount(), thdiff});
    }

    /**
     * Method to create and start a watchdog with provided parameters and a
     * default alert listener that simply increments the {@link #fails}
     * variable.
     *
     * @param timeout
     * @param alertPeriod
     * @param maxAlert
     *
     * @return the watchdog instance
     */
    protected static WatchDog createWatchDog(long timeout, long alertPeriod, int maxAlert) {
        WatchDog wd = null;
        try {
            wd = WatchDog.create("testwd", (int index) -> {
                fails.incrementAndGet();
                LOGGER.log(Level.INFO, "alert index {0}, fails {1}", new Object[]{index, fails});
                // -- will be executed on another thread -- fail("alert");
            }, timeout, alertPeriod, maxAlert);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "fail to start", e);
            fail("fail to start");
        }
        return wd;
    }

    /**
     * Test method for {@link time.WatchDog#ping()}.
     */
    @Test
    public void testPing1() {
        WatchDog wd = createWatchDog(1000, 2000, 10);        
        ScheduledThreadPoolExecutor sched = new ScheduledThreadPoolExecutor(1);

        for (int i = 0; i < ITERATIONS; i++) {
            sched.schedule(() -> {wd.ping(); pings.incrementAndGet();}, 
                    (i + 1) * 990L, TimeUnit.MILLISECONDS);
        }

        sched.shutdown();
        try {
            sched.awaitTermination(ITERATIONS * 2_000L, TimeUnit.MILLISECONDS);
        } catch (InterruptedException ex) {
            // NOOP
        }
        
        wd.stop();

        assertEquals(ITERATIONS, pings.get());
        assertEquals(0, fails.get());
    }

    /**
     * Test method for {@link time.WatchDog#ping()}.
     */
    @Test
    public void testPing2() {
        WatchDog wd = createWatchDog(1000, 2000, 10);
        ScheduledThreadPoolExecutor sched = new ScheduledThreadPoolExecutor(1);

        for (int i = 0; i < ITERATIONS; i++) {
            sched.schedule(() -> {wd.ping(); pings.incrementAndGet();}, 
                    (i + 1) * 1050L, TimeUnit.MILLISECONDS);
        }

        sched.shutdown();
        try {
            sched.awaitTermination(ITERATIONS * 2_000L, TimeUnit.MILLISECONDS);
        } catch (InterruptedException ex) {
            // NOOP
        }

        wd.stop();

        assertEquals(ITERATIONS, pings.get());
        assertEquals(ITERATIONS / 2, fails.get());
    }

    /**
     * Test method for {@link time.WatchDog#ping()}.
     */
    @Test
    public void testPing3() {
        // aggressive ping
        WatchDog wd = createWatchDog(1000, 2000, 10);
        ScheduledThreadPoolExecutor sched = new ScheduledThreadPoolExecutor(1);

        for (int i = 0; i < ITERATIONS * 100; i++) {
            sched.schedule(() -> {wd.ping(); pings.incrementAndGet();}, 
                    (i + 1) * 10L, TimeUnit.MILLISECONDS);
        }

        sched.shutdown();
        try {
            sched.awaitTermination(ITERATIONS * 2_000L, TimeUnit.MILLISECONDS);
        } catch (InterruptedException ex) {
            // NOOP
        }

        assertEquals(ITERATIONS * 100, pings.get());
        assertEquals(0, fails.get());

        wd.stop();
    }

    /**
     * Test method for {@link time.WatchDog#ping()}.
     */
    @Test
    public void testPing4() {
        // even more aggressive ping
        WatchDog wd = createWatchDog(1000, 2000, 10);

        for (int i = 0; i < ITERATIONS * 100; i++) {
            wd.ping();
            pings.incrementAndGet();
        }

        assertEquals(ITERATIONS * 100, pings.get());
        assertEquals(0, fails.get());

        wd.stop();
    }

    private static class PingThread extends Thread {

        WatchDog wd;
        long timeout;
        int runcount;

        public void initpt(WatchDog wd, long timeout) {
            this.initpt(wd, timeout, ITERATIONS * 100);
        }

        public void initpt(WatchDog wd, long timeout, int runcount) {
            this.wd = wd;
            this.timeout = timeout;
            this.runcount = runcount;
        }

        @Override
        public void run() {
            if (this.wd != null) {
                for (int i = 0; i < this.runcount; i++) {
                    if (this.timeout > 0) {
                        try {
                            Thread.sleep(this.timeout);
                        } catch (InterruptedException e) {
                            LOGGER.log(Level.WARNING, "interrupted", e);
                        }
                    }
                    this.wd.ping();
                    pings.incrementAndGet();
                }
            }
        }
    }

    /**
     * Test method for {@link time.WatchDog#ping()}.
     */
    @Test
    public void testPing5() {
        // even more aggressive ping - multithreaded
        WatchDog wd = createWatchDog(1000, 2000, 10);

        PingThread[] thp = new PingThread[100];
        for (int i = 0; i < 100; i++) {
            thp[i] = new PingThread();
            thp[i].initpt(wd, 10);
        }

        for (int i = 0; i < 100; i++) {
            thp[i].start();
        }

        // wait to finish all threads
        for (int i = 0; i < 100; i++) {
            try {
                thp[i].join();
            } catch (InterruptedException e) {
                LOGGER.log(Level.WARNING, "interrupted", e);
            }
        }

        assertEquals(ITERATIONS * 10_000, pings.get());
        assertEquals(0, fails.get());

        wd.stop();
    }

    /**
     * Test method for {@link time.WatchDog#ping()}.
     */
    @Test
    public void testPing6() {
        // even more aggressive ping - insane multithreaded
        WatchDog wd = createWatchDog(1000, 2000, 10);

        PingThread[] thp = new PingThread[100];
        for (int i = 0; i < 100; i++) {
            thp[i] = new PingThread();
            thp[i].initpt(wd, 0, ITERATIONS * 1000);
        }

        for (int i = 0; i < 100; i++) {
            thp[i].start();
        }

        // wait to finish all threads
        for (int i = 0; i < 100; i++) {
            try {
                thp[i].join();
            } catch (InterruptedException e) {
                LOGGER.log(Level.WARNING, "interrupted", e);
            }
        }

        assertEquals(ITERATIONS * 100_000, pings.get());
        assertEquals(0, fails.get());

        wd.stop();
    }

    /**
     * Test method for {@link time.WatchDog#ping()}.
     */
    @Test
    public void testPing7() {
        // fail test
        WatchDog wd = createWatchDog(1000, 2000, 5);

        try {
            Thread.sleep(1000 + 2000 * 4 + 100);
        } catch (InterruptedException e) {
            LOGGER.log(Level.WARNING, "interrupted", e);
        }

        wd.ping();
        pings.incrementAndGet();

        assertEquals(5, fails.get());

        try {
            Thread.sleep(1000 + 2000 * 4 + 100);
        } catch (InterruptedException e) {
            LOGGER.log(Level.WARNING, "interrupted", e);
        }

        wd.ping();
        pings.incrementAndGet();

        assertEquals(10, fails.get());

        wd.stop();
    }

    /**
     * Test method for
     * {@link time.WatchDog#start(time.WatchDog.AlertListener, long, long, int)}.
     */
    @Test
    public void testStart1() {
        WatchDog wd = new WatchDog("testwd");

        // start creates the watch thread and starts it
        try {
            wd.start(null, 1000, 0, 0);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "fail to start", e);
            fail("fail to start");
        }
        assertNotNull(wd.getWatcher());
        assertTrue(wd.getWatcher().isAlive());
        // stop destroys the watch thread
        WatcherTask th = wd.getWatcher();
        wd.stop();
        assertNull(wd.getWatcher());
        assertFalse(th.isAlive());
    }

    /**
     * Test method for
     * {@link time.WatchDog#start(time.WatchDog.AlertListener, long, long, int)}.
     */
    @Test
    public void testStart2() {
        WatchDog wd = new WatchDog("testwd");

        // multiple starts determines that the existing thread is destroyed
        // and a new one is created
        try {
            wd.start(null, 1000, 0, 0);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "fail to start", e);
            fail("fail to start");
        }
        WatcherTask th = wd.getWatcher();
        assertTrue(th.isAlive());

        try {
            wd.start(null, 1000, 0, 0);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "fail to start", e);
            fail("fail to start");
        }
        assertNotSame(th, wd.getWatcher());
        assertTrue(wd.getWatcher().isAlive());
        assertFalse(th.isAlive());

        th = wd.getWatcher();
        wd.stop();
        assertNull(wd.getWatcher());
        assertFalse(th.isAlive());
    }

    /**
     * Test method for
     * {@link time.WatchDog#start(time.WatchDog.AlertListener, long, long, int)}.
     */
    @Test
    public void testStart3() {
        for (int i = 0; i < 100; i++) {
            testStart1();
        }
    }

    /**
     * Test method for
     * {@link time.WatchDog#start(time.WatchDog.AlertListener, long, long, int)}.
     */
    @Test
    public void testStart4() {
        for (int i = 0; i < 100; i++) {
            testStart2();
        }
    }

    /**
     * Test method for
     * {@link time.WatchDog#start(time.WatchDog.AlertListener, long, long, int)}.
     */
    @Test
    public void testStart5() {
        WatchDog wd = new WatchDog("testwd");

        // start creates the watch thread and starts it
        try {
            wd.start(null, 0, 0, 0);
            fail("started with 0 timeout");
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "fail to start", e);
        }
        assertNull(wd.getWatcher());
    }

    /**
     * Test method for {@link time.WatchDog#stop()}.
     */
    @Test
    public void testStop1() {
        WatchDog wd = new WatchDog("testwd");

        // start creates the watch thread and starts it
        try {
            wd.start(null, 1000, 0, 0);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "fail to start", e);
            fail("fail to start");
        }
        assertNotNull(wd.getWatcher());
        assertTrue(wd.getWatcher().isAlive());
        // stop destroys the watch thread
        WatcherTask th = wd.getWatcher();
        wd.stop();
        assertNull(wd.getWatcher());
        assertFalse(th.isAlive());

        // another stop does nothing
        wd.stop();
        assertNull(wd.getWatcher());
    }

    /**
     * Test method for {@link time.WatchDog#stop()}.
     */
    @Test
    public void testStop2() {
        WatchDog wd = new WatchDog("testwd");

        // start creates the watch thread and starts it
        try {
            wd.start((idx) -> wd.stop(), 1000, 1000, 10);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "fail to start", e);
            fail("fail to start");
        }
        assertNotNull(wd.getWatcher());
        assertTrue(wd.getWatcher().isAlive());
        // stop destroys the watch thread
        WatcherTask th = wd.getWatcher();

        // wait for the watchdog thread to stop
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            // noop
        }

        assertNull(wd.getWatcher());
        assertFalse(th.isAlive());
    }

    private static class ShutdownHook extends Thread {

        @Override
        public void run() {
            fails.set(-1);
            System.err.println("shutdown hook: bye");
        }
    }

    /**
     * Test for application shutdown.
     */
    @Test
    public void testShutdown() {

        //fail("run this individually since it will terminate the application");

        //
        // shutdown test
        //
        Thread shook = new ShutdownHook();
        Runtime.getRuntime().addShutdownHook(shook);
        //
        WatchDog wd = createWatchDog(1000, 2000, 5);

        // play with fire
        try {
            Thread.sleep(1000 + 2000 * 5 + 100);
        } catch (InterruptedException e) {
            LOGGER.log(Level.WARNING, "interrupted", e);
        }

        // expect the application to shutdown
        // I don't expect this code will ever run
        // look in the console for the "bye" from shutdown hook

        try {
            shook.join();
        } catch (InterruptedException e) {
            LOGGER.log(Level.WARNING, "interrupted", e);
        }

        //assertEquals(-1, fails.get());

        wd.stop();
    }
}
