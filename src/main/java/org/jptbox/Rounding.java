/*
 * Copyright (c) 2013-2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox;

/**
 * Utility class to allow rounding of numbers to some multiple of initial value.
 * 
 * @author Florin Ganea
 */
public class Rounding {
    
    private final long multiple;
    
    /**
     * Constructor with a long multiple value.
     * <p>
     * The values produced by invoking this instance methods will produce 
     * multiples of the provided value.
     * 
     * @param multiple  the long multiple value
     */
    public Rounding(long multiple) {
        if (multiple < 1) {
            throw new IllegalArgumentException("multiple must be a positive long integer");
        }
        this.multiple = multiple;
    }
    
    /**
     * Returns the smallest multiple of the initial value greater than or 
     * equal to the provided value.
     * 
     * @param value     a long value; better be positive
     * 
     * @return the smallest multiple of the initial value greater than or equal 
     *         to the provided value
     */
    public long top(long value) {
        long result = value;
        
        if (multiple > 1) {
            long rem =  value % multiple;
            if (rem > 0) {
                result += multiple - rem;
            }
        }
        
        return result;
    }

    /**
     * Returns the largest multiple of the initial value smaller than or 
     * equal to the provided value.
     * 
     * @param value     a long value; better be positive
     * 
     * @return the largest multiple of the initial value smaller than or equal 
     *         to the provided value
     */
    public long bottom(long value) {
        long result = value;
        
        if (multiple > 1) {
            long rem = value % multiple;
            if (rem > 0) {
                result -= rem;
            }
        }
        
        return result;
    }
    
}
