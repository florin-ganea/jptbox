/*
 * Copyright (c) 2015-2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox;

import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

/**
 * An implementation of {@link BlockingQueue} that uses non-blocking unbound inner queue
 * and a spinning sleep for implementing the "blocking" part.
 * <p>
 * The latency is about 20ms by default, or it can be specified in the constructor.
 * Latency lower than 10ms is invalid.
 * <p>
 * The goal is to offer superior throughput by avoiding actual blocking.
 * 
 * @param <E> the type of the queued elements
 *
 * @author Florin Ganea
 */
public class SpinBlockingQueue<E> implements BlockingQueue<E> {
    
    private static final long DEFAULT_SPIN_TIME = 20L; // ms
    
    private final long spinTime;
    private final ConcurrentLinkedQueue<E> inner;
    
    {
        inner = new ConcurrentLinkedQueue<>();
    }

    /**
     *
     */
    public SpinBlockingQueue() {
        this(DEFAULT_SPIN_TIME, TimeUnit.MILLISECONDS);
    }
    
    /**
     *
     * @param spinTime
     * @param unit
     */
    public SpinBlockingQueue(long spinTime, TimeUnit unit) {
        this.spinTime = unit.toMillis(spinTime);
        if (spinTime < 10L) {
            throw new IllegalArgumentException("spinTime is lower than 10ms");
        }
    }
    
    @Override
    public boolean add(E e) {
        return inner.add(e);
    }

    @Override
    public boolean offer(E e) {
        return inner.offer(e);
    }

    @Override
    public E remove() {
        return inner.remove();
    }

    @Override
    public E poll() {
        return inner.poll();
    }

    @Override
    public E element() {
        return inner.element();
    }

    @Override
    public E peek() {
        return inner.peek();
    }

    @Override
    public int size() {
        return inner.size();
    }

    @Override
    public boolean isEmpty() {
        return inner.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return inner.contains(o);
    }

    @Override
    public Iterator<E> iterator() {
        return inner.iterator();
    }

    @Override
    public Object[] toArray() {
        return inner.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return inner.toArray(a);
    }

    @Override
    public boolean remove(Object o) {
        return inner.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return inner.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return inner.addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return inner.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return inner.removeAll(c);
    }

    @Override
    public void clear() {
        inner.clear();
    }

    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public void put(E e) throws InterruptedException {
        while (!inner.offer(e)) {
            Thread.sleep(spinTime);
        }
    }

    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public boolean offer(E e, long timeout, TimeUnit unit) throws InterruptedException {
        long sleepTime = unit.toMillis(timeout);
        long expire = System.currentTimeMillis() + sleepTime;
        long timediff = 0L;
        boolean result;
        
        do {
            result = inner.offer(e);
            if (!result) {
                if (timediff == 0L) {
                    // first time; do not take current time again
                    timediff = sleepTime;
                } else {
                    // how much time do we have?
                    timediff = System.currentTimeMillis() - expire;
                }
                if (timediff > 0) {
                    if (timediff < spinTime) {
                        Thread.sleep(timediff);
                    } else {
                        Thread.sleep(spinTime);
                    }
                }
            }
        } while (!result && timediff > 0);
        
        return result;
    }

    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public E take() throws InterruptedException {
        E head;
        
        do {
            head = inner.poll();
            if (head == null) {
                Thread.sleep(spinTime);
            }
        } while (head == null);

        return head;
    }

    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public E poll(long timeout, TimeUnit unit) throws InterruptedException {
        long sleepTime = unit.toMillis(timeout);
        long expire = System.currentTimeMillis() + sleepTime;
        long timediff = 0L;
        E result;
        
        do {
            result = inner.poll();
            if (result == null) {
                if (timediff == 0L) {
                    // first time; do not take current time again
                    timediff = sleepTime;
                } else {
                    // how much time do we have?
                    timediff = System.currentTimeMillis() - expire;
                }
                if (timediff > 0) {
                    if (timediff < spinTime) {
                        Thread.sleep(timediff);
                    } else {
                        Thread.sleep(spinTime);
                    }
                }
            }
        } while (result == null && timediff > 0);
        
        return result;
    }

    @Override
    public int remainingCapacity() {
        // the inner queue is unbounded
        return Integer.MAX_VALUE;
    }

    @Override
    public int drainTo(Collection<? super E> c) {
        E head;
        int cnt = 0;
        
        do {
            head = inner.poll();
            if (head != null) {
                c.add(head);
                cnt++;
            }
        } while (head != null);
        
        return cnt;
    }

    @Override
    public int drainTo(Collection<? super E> c, int maxElements) {
        E head;
        int cnt = 0;
        
        if (maxElements > 0) {
            do {
                head = inner.poll();
                if (head != null) {
                    c.add(head);
                    cnt++;
                }
            } while (head != null && cnt < maxElements);
        }
        
        return cnt;
    }
    
    
}
