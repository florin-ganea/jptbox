/*
 * Copyright (c) 2015-2016, Florin Ganea
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.codec;

/**
 * Base64 data encoding / decoding.
 * <p>
 * Transforms (encodes) data as byte arrays into char array according to Base64
 * encoding rules. The bytes char array has printable ASCII characters: 'A'-'Z',
 * 'a'-'z', '0'-'9', '+', '/', and possibly the special padding character: '='.
 * <p>
 * Transforms (decodes) char arrays representing Base64 encodings into data byte
 * arrays according to the Base64 decoding rules.
 * <p>
 * Standard: RFC 4648.
 * </p>
 * The class instances are thread safe and have no multi-thread contention.
 *
 * @author Florin Ganea
 */
public class Base64 {

    /**
     * maps a 6-bit value (index) to an encoded value
     */
    private static final String base64_alphabet_str
            = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    static final char[] base64_alphabet;
    
    /**
     * base64 padding character
     */
    static final char base64_pad = '=';

    /**
     * maps an encoded value (index) to its 6-bit value
     */
    static final byte[] base64_reverse;

    static {
        // the alphabet
        base64_alphabet = base64_alphabet_str.toCharArray();
        
        // reverse table; basic ASCII is enough
        base64_reverse = new byte[128];
        // init the table
        for (int i = 0; i < base64_reverse.length; i++) {
            // < 0
            base64_reverse[i] = (byte) -1;
        }
        // update only the base64 alphabet positions
        for (int i = 0; i < base64_alphabet.length; i++) {
            base64_reverse[base64_alphabet[i]] = (byte) i;
        }
    }

    /**
     * Encodes a 24-bit quantum. The quantum is in input byte array starting
     * with provided offset and it has 3 bytes, unless the end of the byte array
     * is encountered, case in which at least 1 byte has to be available.
     * <p>
     * Input byte array having <code>null</code> or insufficient elements (at
     * least 1) after the provided offset (including offset out of range) is
     * illegal.
     * <p>
     * Output char array having <code>null</code> or insufficient element (at
     * least 4) after the provided offset (including offset out of range) is
     * illegal.
     *
     * @param input         the input byte array
     * @param inputOffset   the quantum start index in the input byte array
     * @param output        the bytes char array
     * @param outputOffset  the index in bytes char array where the 4 resulting
     *                          characters are written
     */
    private void encodeQuantum(byte[] input, int inputOffset,
            char[] output, int outputOffset) {
        if (input == null) {
            throw new IllegalArgumentException("input is null");
        }

        if (inputOffset < 0 || inputOffset > input.length - 1) {
            throw new IllegalArgumentException("input offset out of range [0 .. len - 1]: " + inputOffset);
        }

        if (output == null) {
            throw new IllegalArgumentException("output is null");
        }

        if (outputOffset < 0 || outputOffset > output.length - 4) {
            throw new IllegalArgumentException("output offset out of range [0 .. len - 4]: " + outputOffset);
        }

        // quantum bit size: 24 bit, 16 bit or 8 bit
        int bitSize = input.length - inputOffset;
        if (bitSize > 3) {
            bitSize = 3;
        }
        bitSize <<= 3; // * 8

        // 6 bit groups
        int group1, group2, group3, group4;

        // extract 6 bit groups from the input byte array
        group1 = (input[inputOffset] & 0xFC) >>> 2;
        group2 = (input[inputOffset] & 0x03) << 4;
        if (bitSize == 8) {
            group3 = -1;
            group4 = -1;
        } else {
            group2 |= (input[inputOffset + 1] & 0xF0) >>> 4;
            group3 = (input[inputOffset + 1] & 0x0F) << 2;
            if (bitSize == 16) {
                group4 = -1;
            } else {
                group3 |= (input[inputOffset + 2] & 0xC0) >>> 6;
                group4 = input[inputOffset + 2] & 0x3F;
            }
        }

        // encode and fill the lbytes
        output[outputOffset] = base64_alphabet[group1];
        output[outputOffset + 1] = base64_alphabet[group2];
        if (group3 < 0) {
            output[outputOffset + 2] = base64_pad;
        } else {
            output[outputOffset + 2] = base64_alphabet[group3];
        }
        if (group4 < 0) {
            output[outputOffset + 3] = base64_pad;
        } else {
            output[outputOffset + 3] = base64_alphabet[group4];
        }
    }

    /**
     * Decodes an 24-bit quantum. The encoding is read from the input char array
     * as a sequence of exactly 4 characters starting with input offset.
     * <p>
     * The decoded bytes are put in the bytes byte array starting with bytes
     * offset and having exactly: 1 byte if the 3rd and 4th characters are
     * padding, 2 bytes if the 4th character is padding or 3 bytes if there is
     * no padding in the input processed sequence.
     * <p>
     * It is assumed that the input char array has at least 4 characters more
     * starting with the offset and the bytes byte array has enough element
     * starting with the provided offset in order to store the decoded bytes.
     *
     * @param input         the input char array with the base64 encoding
     * @param inputOffset   the offset in the input char array where the decoding
     *                          should begin
     * @param output        the bytes byte array to store the decoded content
     * @param outputOffset  the offset in the bytes byte array where the decoded
     *                          content should start
     */
    private void decodeQuantum(char[] input, int inputOffset,
            byte[] output, int outputOffset) {
        
        if (input == null) {
            throw new IllegalArgumentException("input is null");
        }

        if (inputOffset < 0 || inputOffset > input.length - 4) {
            throw new IllegalArgumentException("input offset out of range [0 .. len - 4]: " + inputOffset);
        }

        // 6-bit groups
        int[] group = {-1, -1, -1, -1};
        
        // chars basic checks and reverse values
        for (int i = 0; i < 4; i++) {
            char ch = input[inputOffset + i];
            if (ch <= 0 || ch >= base64_reverse.length
                    || (ch != base64_pad && base64_reverse[ch] < 0)) {
                throw new IllegalArgumentException("invalid character in encoding: " + ch);
            }
            if (ch != base64_pad) {
                group[i] = base64_reverse[ch];
            }
        }

        // padding handling
        int outputBytes = 3;
        if (input[inputOffset + 3] == base64_pad) {
            outputBytes--;
            if (input[inputOffset + 2] == base64_pad) {
                outputBytes--;
            }
        }

        if (outputOffset < 0 || outputOffset > output.length - outputBytes) {
            throw new IllegalArgumentException("output offset out of range [0 .. len - ?]: " + outputOffset);
        }
        
        // transforming from 6-bit groups in bytes (8-bit values)
        switch (outputBytes) {
            case 3:
                output[outputOffset + 2] = (byte) (((group[2] & 0x03) << 6) | group[3]);
                // no break
            case 2:
                output[outputOffset + 1] = (byte) (((group[1] & 0x0F) << 4) | (group[2] >>> 2));
                // no break
            case 1:
                output[outputOffset] = (byte) ((group[0] << 2) | (group[1] >>> 4));
                // no break
        }
    }

    /**
     * Encodes a byte array in Base64 encoding.
     * <p>
     * 0-length byte array is encoded as 0-length char array.
     * <p>
     * <code>null</code> data byte array is illegal.
     *
     * @param data  the byte array to be encoded
     * @return      the base64 encoding of the input byte array as char array
     */
    public char[] encode(byte[] data) {
        char[] output;
        int outputLength;
        int outputOffset;

        if (data == null) {
            throw new IllegalArgumentException("data byte array is null");
        }

        outputLength = data.length / 3;
        if (data.length % 3 > 0) {
            outputLength++;
        }
        outputLength <<= 2; // * 4
        output = new char[outputLength];
        outputOffset = 0;

        for (int offset = 0; offset < data.length; offset += 3) {
            encodeQuantum(data, offset, output, outputOffset);
            outputOffset += 4;
        }

        return output;
    }

    /**
     * Decodes a byte64 encoding in a byte array.
     * <p>
     * 0-length encoding char array is decoded as 0-length byte array.
     * <p>
     * <code>null</code> encoding char array is illegal.
     *
     * @param encoding  the base64 encoding as char array
     * @return          the decoded byte array
     */
    public byte[] decode(char[] encoding) {
        
        if (encoding == null) {
            throw new IllegalArgumentException("encoding char array is null");
        }
        
        if (encoding.length % 4 != 0) {
            throw new IllegalArgumentException("encoding is malformed");
        }

        byte[] data;
        int dataLen = encoding.length / 4 * 3;
        int dataOffset;
        
        if (encoding.length > 2) {
            if (encoding[encoding.length - 1] == base64_pad) {
                dataLen--;
                
                if (encoding[encoding.length - 2] == base64_pad) {
                    dataLen--;
                }
            }
        }
        
        data = new byte[dataLen];
        dataOffset = 0;
        
        for (int offset = 0; offset < encoding.length; offset += 4) {
            decodeQuantum(encoding, offset, data, dataOffset);
            dataOffset += 3;
        }
        
        return data;
    }

    /**
     * Encodes a 64-bit <code>long</code> primitive value into a String using
     * Base64 encoding.
     * <p>
     * The encoding has exactly 12 char length.
     * <p>
     * Note: JVM uses big-endian data representation.
     *
     * @param value the value to be encoded
     * @return      a String representing the Base64 encoding of the value
     * 
     * @see Base64#encode(byte[]) 
     * @see Base64#fastEncode(long) 
     */
    public String encode(long value) {
        byte[] lbytes = new byte[Long.BYTES];
        long lvalue = value;

        for (int i = Long.BYTES - 1; i >= 0; i--) {
            lbytes[i] = (byte) lvalue;
            lvalue >>>= 8;
        }

        return String.valueOf(encode(lbytes));
    }

    /**
     * Decodes a Base64 encoded String into a 64-bit <code>long</code> primitive
     * value.
     * <p>
     * The encoding has to be 12 chars length with only one padding char. Other
     * values, including <code>null</code> are illegal.
     * <p>
     * Note: JVM uses big-endian data representation.
     *
     * @param encoding  the Base64 encoding as String
     * @return          the decoding as <code>long</code> primitive value.
     * 
     * @see Base64#fastDecodeLong(java.lang.String) 
     * @see Base64#decode(char[]) 
     */
    public long decodeLong(String encoding) {
        byte[] lbytes;
        long lvalue;

        if (encoding == null) {
            throw new IllegalArgumentException("encoding is null");
        }

        if (encoding.length() != 12 || encoding.indexOf(base64_pad) != 11) {
            throw new IllegalArgumentException("encoding is not a long value");
        }

        lbytes = this.decode(encoding.toCharArray());

        if (lbytes.length != Long.BYTES) {
            // very bad; decoding error...
            throw new IllegalArgumentException("encoding is not a long value; decode failed");
        }

        lvalue = lbytes[0];
        for (int i = 1; i < Long.BYTES; i++) {
            lvalue <<= 8;
            lvalue |= lbytes[i] & 0xFF;
        }

        return lvalue;
    }

    /**
     * Encodes a 64-bit <code>long</code> primitive value into a String using
     * Base64 encoding using a faster, direct algorithm.
     * <p>
     * Instead of converting into byte array then group the bytes in 6-bit,
     * this implementation converts directly the long primitive value in 6-bit 
     * groups.
     * <p>
     * The encoding has exactly 12 char length.
     * <p>
     * Note: JVM uses big-endian data representation.
     *
     * @param value the value to be encoded
     * @return      a String representing the Base64 encoding of the value
     * 
     * @see Base64#fastEncode(long) 
     * @see Base64#encode(long) 
     */
    public String fastEncode(long value) {
        long lvalue = value; // 64-bit == 10 x 6-bit + 4-bit
        char[] encoding = new char[12]; // 10 full + 1 partial + 1 padding
        
        // padding
        encoding[11] = base64_pad;
        // partial (last 4-bit)
        encoding[10] = base64_alphabet[((int) (lvalue & 0x0F)) << 2];
        lvalue >>>= 4;
        
        // 10 full encoded values (6-bit groups)
        for (int i = 9; i >= 0; i--) {
            encoding[i] = base64_alphabet[(int) (lvalue & 0x3F)];
            lvalue >>>= 6;
        }
        
        return new String(encoding);
    }
    
    /**
     * Decodes a Base64 encoded String into a 64-bit <code>long</code> primitive
     * value using a faster, direct algorithm.
     * <p>
     * The groups of 6-bit are directly converted into the primitive value 
     * instead of converting them to char array, then pass them to general 
     * decode algorithm, then convert the decoded byte array in the primitive 
     * value.
     * <p>
     * The encoding has to be 12 chars length with only one padding char. Other
     * values, including <code>null</code> are illegal.
     * <p>
     * Note: JVM uses big-endian data representation.
     *
     * @param encoding  the Base64 encoding as String
     * @return          the decoding as <code>long</code> primitive value.
     * 
     * @see Base64#decodeLong(java.lang.String) 
     * @see Base64#decode(char[]) 
     */
    public long fastDecodeLong(String encoding) {
        long lvalue;

        if (encoding == null) {
            throw new IllegalArgumentException("encoding is null");
        }

        if (encoding.length() != 12 || encoding.indexOf(base64_pad) != 11) {
            throw new IllegalArgumentException("encoding is not a long value");
        }
        
        lvalue = 0;
        // 10 full 6-bit groups
        for (int i = 0; i < 10; i++) {
            if (i > 0) { 
                lvalue <<= 6;
            }
            lvalue |= base64_reverse[encoding.charAt(i)];
        }

        // 1 partial 4-bit
        lvalue <<= 4;
        lvalue |= (base64_reverse[encoding.charAt(10)] >>> 2);
        
        return lvalue;
    }
    
}
