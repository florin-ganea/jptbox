/*
 * Copyright (c) 2013-2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.statistics;

import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Processing counter class with generic parameter for id-s.
 * 
 * @author Florin Ganea
 * 
 * @param <T> the type of the id that is used to match a begin with an end
 */
public class ProcessingCounter<T> {

    private static final Logger log = Logger.getLogger(ProcessingCounter.class.getPackage().getName());

    private long min;
    private long max;
    private double avg;
    private long avg_cnt;
    private long timeout_cnt;

    private T minId;
    private T maxId;
    private T firstTimeoutId;
    
    private final Object monitor;

    /**
     * the name of the performance counter; used in string representation
     */
    protected final String name;

    /**
     * whether the counter is enabled (updates are taken into account) or not
     */
    protected volatile boolean enabled;

    /**
     * the map with id-s and the registered start processing time
     */
    protected final ConcurrentHashMap<T, Long> ids;

    {
        this.enabled = true;
        this.monitor = new Object();
        this.ids = new ConcurrentHashMap<>(256);
        
        this.min = 0;
        this.max = 0;
        this.minId = null;
        this.maxId = null;
        this.firstTimeoutId = null;
        this.avg = 0;
        this.avg_cnt = 0;
        this.timeout_cnt = 0;
    }
    
    /**
     * Constructs a performance counter with the given name.
     * 
     * @param counterName the performance counter name
     */
    public ProcessingCounter(String counterName) {
        this.name = counterName;
    }

    /**
     * Disables the performance counter; operations do not have any effect.
     */
    public void disable() {
        this.enabled = false;
    }

    /**
     * Enables the performance counter; operations are updating the counter 
     * state.
     */
    public void enable() {
        this.enabled = true;
    }

    /**
     * Resets all information stored by this counter to the starting values.
     * The mapping of id-s to their start times is also cleared.
     */
    public void reset() {
        this.ids.clear();
        synchronized (monitor) {
            this.min = 0;
            this.max = 0;
            this.minId = null;
            this.maxId = null;
            this.firstTimeoutId = null;
            this.avg = 0;
            this.avg_cnt = 0;
            this.timeout_cnt = 0;
        }
    }

    private void update(long time, T id) {
        if (!this.enabled) {
            return;
        }
        //
        synchronized (monitor) {
            if (this.min == 0 || this.min > time) {
                this.min = time;
                this.minId = id;
            }

            if (this.max == 0 || this.max < time) {
                this.max = time;
                this.maxId = id;
            }

            this.avg = (this.avg * this.avg_cnt + time) / (this.avg_cnt + 1);
            this.avg_cnt++;
        }
    }

    /**
     * Registers an id as its processing starts in this moment.
     * 
     * @param id    the id of the task that begins to be processed
     * 
     * @return the current time, as it was registered for this processing start
     */
    public long beginProcessing(T id) {
        long time = 0;
        //
        if (!this.enabled) {
            return time;
        }
        //
        time = System.currentTimeMillis();
        Long exTime = this.ids.putIfAbsent(id, Long.valueOf(time));
        if (exTime != null) {
            Exception ex = new Exception("begin processing with in-progress id: " + id);
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        //
        return time;
    }

    /**
     * Marks a processing task as ended. The start time is taken from the
     * mapping and the processing time is calculated and internal data is 
     * updated.
     *
     * @param id    the id of the task being ended
     * 
     * @return  the current time, as it was taken for this processing end
     */
    public long endProcessing(T id) {
        long time = 0;
        //
        if (!this.enabled) {
            return time;
        }
        //
        Long exTime = this.ids.remove(id);
        if (exTime == null) {
            Exception ex = new Exception("end processing with not exiting id: " + id);
            log.log(Level.WARNING, ex.getMessage(), ex);
        } else {
            time = System.currentTimeMillis();
            this.update(time - exTime, id);
        }
        //
        return time;
    }

    /**
     * Marks the task with the given id as timed out. As consequence, the task
     * is not taken into account in regular counter calculations, but a special 
     * count for the timed out tasks is incremented.
     * 
     * @param id    the id of the task being marked as timed out
     */
    public void timeout(T id) {
        //
        if (!this.enabled) {
            return;
        }
        //
        Long exTime = this.ids.remove(id);
        if (exTime == null) {
            Exception ex = new Exception("timeout with not exiting id: " + id);
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        synchronized (monitor) {
            if (firstTimeoutId == null) {
                firstTimeoutId = id;
            }
            timeout_cnt++;
        }
        //
    }

    @Override
    public String toString() {
        long lmin;
        long lmax;
        double lavg;
        long lavg_cnt;
        long ltimeout_cnt;
        //
        T lminId;
        T lmaxId;
        T lfirstTimeoutId;

        StringBuilder sb = new StringBuilder(256);
        sb.append(this.name);

        synchronized (monitor) {
           lmin = this.min;
           lmax = this.max;
           lavg = this.avg;
           lavg_cnt = this.avg_cnt;
           ltimeout_cnt = this.timeout_cnt;
           //
           lminId = this.minId;
           lmaxId = this.maxId;
           lfirstTimeoutId = this.firstTimeoutId;
        }

        sb.append(": avg = ").append(SimpleDecimalFormat.format(lavg)).append(" ms (cnt = ").append(lavg_cnt).append(')');
        sb.append(", min = ").append(lmin).append(" ms (id = ").append(lminId).append(')');
        sb.append(", max = ").append(lmax).append(" ms (id = ").append(lmaxId).append(')');
        sb.append(", tmout = ").append(ltimeout_cnt);
        if (lfirstTimeoutId != null) {
            // add it only if relevant
            sb.append(" (first = ").append(lfirstTimeoutId).append(')');
        }

        sb.append(", in progress = ").append(this.ids.size());

        return sb.toString();
    }

}
