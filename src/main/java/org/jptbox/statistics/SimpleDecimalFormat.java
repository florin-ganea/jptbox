/*
 * Copyright (c) 2013, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.statistics;


/**
 * Simple floating point formatter that can be used to limit and a bit customize
 * the number of decimals.
 * <p>
 * And... hey! It is thread-safe, not synchronized and it makes rounding to the
 * nearest instead of the standard Java rounding half-up.
 * <p>
 * And it is also 30% faster than the standard Java counterpart.
 *
 * @author Florin Ganea
 * @version 1.0 $
 */
public class SimpleDecimalFormat {

    /**
     * Format a double value with the specified number of decimals.
     * <p>
     * There is a rounding to nearest done at the last decimal.
     * <p>
     * If <code>lastDecimal0</code> is <code>true</code> the last decimal is always 0, 
     * rounding is being made at the previous one.
     * 
     * @param value
     * @param decimals
     * @param lastDecimal0
     * @return
     */
    public static String format(double value, int decimals, boolean lastDecimal0) {
        double v = value;
        int d = decimals;
        boolean negative = false;
        
        if (v < Long.MIN_VALUE || v > Long.MAX_VALUE) {
            throw new IllegalArgumentException("value overflow");
        }
        
        if (d < 0) {
            throw new IllegalArgumentException("negative number of decimals");
        }
        
        if (d == 0) {
            if (lastDecimal0) {
                throw new IllegalArgumentException("last decimal cannot be 0 when there are no decimals");
            }
        }
        
        if (lastDecimal0) {
            d--;
        }
        
        if (v < 0) {
            negative = true;
            v = -v;
        }
        
        v = Math.round(v * Math.pow(10, d)) / Math.pow(10, d);
        
        if (lastDecimal0) {
            d++;
        }

        StringBuilder sb = new StringBuilder(64);

        if (negative) {
            sb.append('-');
        }
        sb.append(v);

        int ppos = sb.indexOf(".");

        if (ppos > 0) {
            if (d == 0) {
                sb.setLength(ppos);
            } else {
                d = d - sb.length() + ppos + 1;
            }
        } else {
            // dot not there; weird...
            if (d > 0) {
                sb.append('.');
            }
        }

        while (d > 0) {
            sb.append('0');
            d--;
        }

        return sb.toString();
    }

    /**
     * Format a double value with the specified number of decimals.
     * <p>
     * There is a rounding to nearest done at the last decimal.
     * 
     * @param value
     * @param decimals
     * @return
     */
    public static String format(double value, int decimals) {
        return format(value, decimals, false);
    }
    
    /**
     * Format a double value with 2 decimals. Example: 2.32.
     * <p>
     * There is a rounding to nearest done at the last decimal.
     * 
     * @param value
     * @return
     */
    public static String format(double value) {
        return format(value, 2, false);
    }

}
