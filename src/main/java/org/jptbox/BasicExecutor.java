/*
 * Copyright (c) 2014, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import org.jptbox.atomic.RoundRobinInt;
import org.jptbox.atomic.Sequencer;

/**
 * Simple Executor that has a number of internal threads able to execute tasks
 * in parallel.
 * <p>
 * The submitted tasks are distributed among internal threads in Round-Robin
 * manner.
 *
 * @author Florin Ganea
 * @version 1.0
 */
public class BasicExecutor implements Executor {

    /** the array of internal threads */
    private final BasicExecutorThread<Runnable>[] threads;
    
    /** the array of queues corresponding to each internal thread; 
     * may contain the same queue reference */
    private final BlockingQueue<Runnable>[] queues;
    
    /** flag to determine the running status and to avoid improper 
     * start / shutdown sequence */
    private final AtomicBoolean running;
    
    /** Round-Robin sequencer */
    private final Sequencer roundRobin;
    
    {
        this.running = new AtomicBoolean(false);
    }
    
    /**
     * Constructs a basic executor instance having a specified number of internal
     * threads with the option to have a shared queue or a queue for each thread.
     * 
     * @param threadCount    the number of internal threads
     * @param queuePerThread whether each thread should have own queue 
     *                       (<code>true</code>) or they share a single queue
     *                       (<code>false</code>)
     * @param favorThroughput when set to <code>true</code> the queues are
     *                       set to a special implementation, non-blocking with 
     *                       spinning sleep
     * 
     * @throws IllegalArgumentException when the threadCount is zero or negative
     */
    public BasicExecutor(int threadCount, boolean queuePerThread, boolean favorThroughput) {
        BlockingQueue<Runnable> queue = null;
        
        if (threadCount <= 0) {
            throw new IllegalArgumentException("negative or zero thread count");
        }
        
        this.threads = new BasicExecutorThread[threadCount];
        this.queues = new BlockingQueue[threadCount];
                
        for (int i = 0; i < threadCount; i++) {
            if (queue == null || queuePerThread) {
                if (favorThroughput) {
                    queue = new SpinBlockingQueue<>();
                } else {
                    queue = new LinkedBlockingQueue<>();
                }
            }
            this.queues[i] = queue;
            this.threads[i] = new BasicExecutorThread<>(queue);
        }
        
        this.roundRobin = new RoundRobinInt(0, threadCount - 1);
    }
    
    /**
     * Start internal threads and waits for all of them to be running.
     * 
     * @throws IllegalStateException        when the executor is already started
     * @throws IllegalThreadStateException  when the executor was shutdown
     */
    public void start() {
        if (this.running.compareAndSet(false, true)) {
            for (BasicExecutorThread<Runnable> thread : this.threads) {
                thread.start();
            }
            // use separate loop for waiting for letting threads unfold in parallel
            for (BasicExecutorThread<Runnable> thread : this.threads) {
                thread.waitRunning();
            }
        } else {
            throw new IllegalStateException("already running");
        }
    }
    
    /**
     * Stops internal threads and waits all of them to be terminated.
     * 
     * @throws IllegalStateException when the executor was not started or is already shutdown
     */
    public void shutdown() {
        if (this.running.compareAndSet(true, false)) {
            for (BasicExecutorThread<Runnable> thread : this.threads) {
                thread.shutdown();
            }
            for (BasicExecutorThread<Runnable> thread : this.threads) {
                thread.waitTerminated(0);
            }
        } else {
            throw new IllegalStateException("not started or already shutdown");
        }
    }
    
    /**
     * Internal method, that can be overridden, used to calculate the thread 
     * index that will execute the provided task.
     * <p>
     * The default implementation is using Round-Robin algorithm among the 
     * internal threads.
     * 
     * @param task the task that is to be executed
     * 
     * @return the index of the thread that should execute the provided task
     */
    protected int getThreadIndex(Runnable task) {
        return roundRobin.getNextInt();
    }
    
    /**
     * Executes the provided task asynchronously on one of the internal threads.
     * 
     * @param task the task to be executed
     * 
     * @throws RejectedExecutionException when the selected queue is full
     * @throws NullPointerException       when the task is null
     */
    @Override
    public void execute(Runnable task) {
        int threadIndex;
        
        if (task == null) {
            throw new NullPointerException("the task is null");
        }
        
        threadIndex = getThreadIndex(task);
        
        if (!this.queues[threadIndex].offer(task)) {
            throw new RejectedExecutionException("queue is full");
        }
        
    }
    
}
