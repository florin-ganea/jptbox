/*
 * Copyright (c) 2013-2014, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Base class for threads that are executing iterations.
 * <p>
 * The number of iterations can be limited or unlimited.
 * <p>
 * The implementation of the {@link Thread#run()} method simply calls the
 * {@link #runIteration()} method either indefinitely (when the maximum number
 * of iterations is unlimited) or exactly the specified number of times.
 * <p>
 * The thread can be stopped at any time by calling {@link #shutdown()} method.
 *
 * @author Florin Ganea
 * @version 1.0
 */
public abstract class IterativeThread extends BasicThread {

    /** Internal logger reference. */
    private static final Logger LOG = Logger.getLogger(IterativeThread.class.getPackage().getName());
    
    /** The maximum numbers of iterations of this thread. {@code 0} means
     * unlimited. */
    protected final long maxIterations;

    /**
     * Constructs a new iterative thread instance with default name and
     * unlimited iterations.
     * <p>
     * The default thread name is {@link BasicThread#DEFAULT_THREAD_NAME} + generated thread index.
     */
    public IterativeThread() {
        this(0L);
    }

    /**
     * Constructs a new iterative thread instance with default name and possible
     * limited iterations, as specified by the provided parameter.
     * <p>
     * The default thread name is {@link BasicThread#DEFAULT_THREAD_NAME} + generated
     * thread index.
     *
     * @param maxIterations the maximum number of iterations this thread is
     *                      expected to run; 0 or negative value means unlimited
     */
    public IterativeThread(long maxIterations) {
        this(BasicThread.DEFAULT_THREAD_NAME, maxIterations);
    }

    /**
     * Constructs a new iterative thread instance with the provided name and
     * unlimited iterations.
     * <p>
     * The actual thread name is formed by the provided name + "-" + generated
     * thread index.
     *
     * @param name the iterative thread name
     */
    public IterativeThread(String name) {
        this(name, 0);
    }

    /**
     * Constructs a new iterative thread instance with the provided name and
     * possible limited iterations, as specified by the provided parameter.
     * <p>
     * The actual thread name is formed by the provided name + "-" + generated
     * thread index.
     *
     * @param name          the iterative thread name
     * @param maxIterations the maximum number of iterations this thread is
     *                      expected to run; 0 or negative value means unlimited
     */
    public IterativeThread(String name, long maxIterations) {
        super(name);
        this.maxIterations = maxIterations;
    }

    /**
     * Initialization part of the iterative thread. Does nothing.
     * 
     * @see BasicThread#initialize() 
     */
    @Override
    @SuppressWarnings("NoopMethodInAbstractClass")
    public void initialize() {
        // noop
    }
    
    
    /**
     * Termination part of the iterative thread. Does nothing.
     * 
     * @see BasicThread#terminate() 
     */
    @Override
    @SuppressWarnings("NoopMethodInAbstractClass")
    public void terminate() {
        // noop
    }
    
    /**
     * Implementation of the {@link BasicThread#execute() ()} method that simply 
     * calls the {@link #runIteration()} method either indefinitely (when the 
     * maximum number of iterations is unlimited) or exactly the specified 
     * number of times.
     * 
     * @see BasicThread#execute() 
     */
    @Override
    public void execute() {
        // the current number of iterations
        long iterations = 0;

        while (this.isRunning()
                && (this.maxIterations <= 0 || iterations < this.maxIterations)) {
            //
            try {
                this.runIteration();
                //
                iterations++;
            }
            catch (InterruptedException ex) {
                // wait for the graceful shutdown
                LOG.log(Level.INFO, "thread interrupted: {0}", this.getName());
            }
        }

        LOG.log(Level.INFO, "executed iterations: {0}", iterations);
    }
    
    /**
     * The iterative thread is supposed to run this method indefinitely or for a
     * maximum number of times, when specified so.
     * 
     * @throws java.lang.InterruptedException
     */
    public abstract void runIteration() throws InterruptedException;

}
