/*
 * Copyright (c) 2014-2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.wd;

import java.util.logging.Level;

/**
 * Internal watchdog wake-up task.
 * <p>
 * It uses the actual parameters from the watchdog to perform its
 * functionalities. In this way, the master class is simply a wrapper to
 * this thread with the purpose to avoid unwanted interactions (like
 * accidentally stopping or interrupting the thread).
 */
class WatcherTask implements Runnable {

    /** Internal running flag; used to shutdown the watchdog. */
    private volatile boolean running;
    
    /** Reference of the object used for alert notifications. */
    private final AlertListener alertListener;
    
    /** The timeout for entering in the alert state. */
    private final long timeout;
    
    /** The alert period for notifications. */
    private final long alertPeriod;
    
    /** After this number consecutive alert intervals the application
     * should be terminated.
     * Actually the monitoring thread throws {@link RuntimeException}. */
    private final int alertMax;
    
    /** The timestamp of the latest ping (dog feeding). */
    private volatile long ping;
    
    {
        this.ping = System.currentTimeMillis();
        this.running = true;
    }

    /**
     * Constructor for the watchdog thread.
     *
     * @param alertListener the alert listener
     * @param timeout       the timeout for entering in the alert state
     * @param alertPeriod   the period for alert notifications
     * @param alertMax      after this number consecutive alert intervals
     *                      the application should be terminated
     *
     * @throws IllegalArgumentException in case the parameters have other
     *                      values than expected (negative timeout,
     *                      alertPeriod or alertMax, 0 timeout)
     */
    public WatcherTask(AlertListener alertListener, long timeout, long alertPeriod, int alertMax) {
        // checks the parameters and set internal members
        this.alertListener = alertListener;
        
        if (timeout <= 0L) {
            throw new IllegalArgumentException("timeout parameter is negative or zero");
        }
        this.timeout = timeout;
        
        if (alertPeriod < 0L) {
            throw new IllegalArgumentException("alertPeriod parameter is negative");
        }
        this.alertPeriod = alertPeriod;
        
        if (alertMax < 0) {
            throw new IllegalArgumentException("alertMax parameter is negative");
        }
        // safety change in order to avoid "wait forever"
        if (alertPeriod == 0L && alertMax > 0) {
            // ignore the max and shutdown on the first alert
            // never do "wait forever" in the watchdog
            // this does not trigger any notification
            // do not issue the warning if the alert max is already zero
            // because it means it was intentionally set so
            WatchDog.LOGGER.log(Level.WARNING, "alert period is zero; setting max attempts to zero");
            this.alertMax = 0;
        } else {
            this.alertMax = alertMax;
        }
    }

    /**
     * Ping (feed) the watchdog.
     */
    public void ping() {
        this.ping = System.currentTimeMillis();
    }

    /**
     * Stops the watchdog; no more reschedules.
     */
    public void shutdown() {
        this.running = false;
    }

    public boolean isAlive() {
        return this.running;
    }

    /**
     * Implementation of the watchdog periodic wakeup routine.
     *
     * @see Runnable#run()
     */
    @Override
    public void run() {
        // check if the watchdog is still running
        if (!this.running) {
            WatchDog.LOGGER.log(Level.SEVERE, "watchdog terminated");
            return;
        }
        // time delta to the next required ping...
        long deltaT = this.ping + this.timeout - System.currentTimeMillis();
        // check the timeouts
        if (deltaT > 0) {
            // dog was fed; go to sleep up to the next timeout
            WatchDog.watcher.schedule(this, deltaT);
        } else {
            // problem; calculate how bad is the problem (alert index)
            long alertIdx;
            long alertDeltaT;
            
            if (this.alertPeriod > 0) {
                // this algorithm assumes the first alert index is 0
                // avoid integral division and reminder of negative numbers!
                alertIdx = (-deltaT) / this.alertPeriod;
                alertDeltaT = -(-deltaT % this.alertPeriod) + this.alertPeriod;
            } else {
                // terminate on first alert
                alertIdx = 0;
                alertDeltaT = 0;
            }
            
            // check if the max alert intervals are exceeded
            if (alertIdx >= this.alertMax) {
                // "Sufficient to the day is its own evil." (Matthew 6.34)
                WatchDog.LOGGER.log(Level.SEVERE, 
                        "watchdog not notified within expected time period; terminating the application");
                // nice way
                throw new RuntimeException("watchdog not notified within expected time period");
                // ugly, but the right way
                //System.exit(0);
            }
            
            // schedule this watcher up to the next alert interval
            WatchDog.watcher.schedule(this, alertDeltaT);
            // alert immediately on a separate thread
            WatchDog.alerter.schedule(
                    () -> {
                        try {
                            this.alertListener.alert((int) alertIdx);
                        } catch (Exception th) {
                            WatchDog.LOGGER.log(Level.SEVERE, "alert listener call failed", th);
                        }
                    },
                    0L);
        }
    }
    
}
