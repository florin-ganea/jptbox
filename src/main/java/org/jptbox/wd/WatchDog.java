/*
 * Copyright (c) 2013-2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.wd;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jptbox.BasicScheduler;

/**
 * The implementation of standard watchdog timer (WDT).
 * <p>
 * The watchdog is an auxiliary component that waits to be notified from time to
 * time that the application is still running properly. If the notification is
 * not received, it enters in the alert state and calls the alert method on the
 * listener (if there is such listener registered). It stays in the alert state
 * and keeps calling the listener method at regular intervals, but no more than
 * a configured maximum. If maximum of alert intervals is reached, the watchdog
 * terminates the application.
 * <p>
 * The watchdog can only be started and notified; it should not be stopped by
 * application, although a stop method is provided. The scope of the stop method
 * is only testing.
 * <p>
 * The watchdog watcherTask cannot be interrupted (it simply ignores the interrupt
 * exceptions).
 * <p>
 * It is not important that the watchdog deals with time very precisely, but it
 * is critical it fulfills its purpose eventually.
 * <p>
 * Parameters of the watchdog:
 * <ul>
 * <li>alert listener - the listener that is used for alert notifications; can
 * be {@code null}</li>
 * <li>timeout - the timeout in milliseconds for the watchdog to enter in alert
 * state; cannot be less or equal to {@code 0}</li>
 * <li>alertPeriod - the period used for sending the alert notifications when
 * the watchdog is in alert state; cannot be less than {@code 0}; a value of
 * {@code 0} means that the watchdog terminates the application on alert without
 * any notification (listener call)</li>
 * <li>alertMax - the maximum number of alert intervals until the watchdog
 * terminates the application; cannot be less than {@code 0}; a value of
 * {@code 0} means that the application is terminated on alert without any
 * notification (listener call)</li>
 * </ul>
 * <p>
 * For more details see Wikipedia
 * <a href="http://en.wikipedia.org/wiki/Watchdog_timer">Watchdog timer</a>
 * page.
 * <p>
 * Also good, funny, true literature:
 * <a href="http://www.ganssle.com/watchdogs.pdf">Great Watchdogs</a>.
 * 
 * @author Florin Ganea
 * @version 1.0
 */
public final class WatchDog {

    /** Internal logger reference. */
    static final Logger LOGGER = Logger.getLogger(WatchDog.class.getName());
    
    static final BasicScheduler alerter;
    static final BasicScheduler watcher;
    
    private static final ConcurrentMap<String, WatchDog> wdogs;
    
    static {
        alerter = new BasicScheduler("WDA");
        watcher = new BasicScheduler("WDT");
        // let the threads be started
        watcher.schedule(() -> {}, 0L);
        alerter.schedule(() -> {}, 0L);
        // watchdogs map
        wdogs = new ConcurrentHashMap<>();
    }
    
    static void shutdown() {
        alerter.shutdownNow();
        watcher.shutdownNow();
    }
    
    /**
     *
     * @param name
     * @param alertListener
     * @param timeout
     * @param alertPeriod
     * @param alertMax
     * @return
     */
    public static WatchDog create(String name, AlertListener alertListener, 
                                  long timeout, long alertPeriod, int alertMax) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("name is null or empty");
        }

        if (wdogs.containsKey(name)) {
            throw new IllegalArgumentException("name " + name + " is already registered");
        }
        
        WatchDog wd = new WatchDog(name);
        wd.start(alertListener, timeout, alertPeriod, alertMax);
        
        return wd;
    }
    
    /**
     *
     * @param name
     * @return
     */
    public static WatchDog get(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("name is null or empty");
        }

        WatchDog wd = wdogs.get(name);
        
        if (wd == null) {
            throw new IllegalArgumentException("name " + name + " is not registered");
        }
     
        return wd;
    }
    
    /**
     *
     */
    public final String name;
    
    /** Internal reference of the watchdog watcherTask. */
    private volatile WatcherTask watcherTask;
    
    {
        this.watcherTask = null;
    }
    
    WatchDog(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("name is null or empty");
        }
        this.name = name;
    }

    /**
     * Sends a ping to the watchdog with the meaning - "relax, master is at
     * home". :-)
     */
    public void ping() {
        WatcherTask threadRef = this.watcherTask;
        //
        if (threadRef != null) {
            // signals the watchdog that everything is cool
            // harmless if the watcherTask is not anymore alive
            threadRef.ping();
            LOGGER.log(Level.FINEST, "{0}: watchdog thread ping called", this.name);
        } else {
            LOGGER.log(Level.SEVERE, "{0}: watchdog thread undefined", this.name);
        }
    }

    /**
     * Starts the watchdog with an alert listener, a timeout and an alert
     * period.
     * <p>
     * If the watchdog awakes (enters the alarm state) the alert method are
     * called periodically until the watchdog is pinged. After a maximum
     * (configurable) consecutive number of alerts the application is
     * terminated.
     *
     * @param alertListener the alert listener; can be <tt>null</tt>
     * @param timeout       the timeout for awaking the watchdog; cannot be less
     *                      or equal to 0
     * @param alertPeriod   the alert period for notifications; cannot be less
     *                      than 0; 0 means immediate termination of application
     *                      in case of watchdog alert
     * @param alertMax      after this number consecutive alerts the application
     *                      is terminated; cannot be less than 0; 0 means
     *                      immediate termination of application in case of
     *                      watchdog alert
     *
     * @throws IllegalArgumentException in case the parameters have other values 
     *                      than expected (negative timeout, alertPeriod or 
     *                      alertMax, 0 timeout)
     */
    void start(AlertListener alertListener, long timeout, long alertPeriod, int alertMax) {
        WatcherTask threadRef;
    
        // make sure the watchdog watcherTask is stopped
        this.stop();

        synchronized (this) {
            if (this.watcherTask == null) {
                // create a new watchdog watcherTask
                threadRef = new WatcherTask(alertListener, timeout, alertPeriod, alertMax);
                this.watcherTask = threadRef;
            } else {
                // already created
                threadRef = null;
            }
        }
        
        // avoid multiple scheduling
        if (threadRef != null) {
            // sleep until next wakeup time
            watcher.schedule(threadRef, timeout);
            LOGGER.log(Level.INFO, "{0}: watchdog thread created and started", this.name);
        }

        wdogs.put(this.name, this);
    }

    /**
     * Stops the watchdog; used for testing / internal purposes.
     * <p>
     * <b>Do not use in applications!</b>
     */
    void stop() {

        WatcherTask threadRef;

        // copy and reset the reference
        // avoids double processing of stop
        synchronized (this) {
            threadRef = this.watcherTask;
            this.watcherTask = null;
        }

        // stop the running watcherTask if any
        if (threadRef != null) {
            threadRef.shutdown();
        } else {
            LOGGER.log(Level.INFO, "{0}: watchdog thread undefined", this.name);
        }

        wdogs.remove(this.name);
        
    }
    
    /**
     * Gets the internal watcher reference.
     * <p>
     * Useful for testing the running status
     *
     * @return the internal watchdog wake-up task reference
     */
    WatcherTask getWatcher() {
        return this.watcherTask;
    }
}
