/*
 * Copyright (c) 2014-2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.atomic;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

/**
 * Non-blocking thread-safe implementation of limited up-down-counter for
 * {@code int}.
 * <p>
 * The counting up or down is performed atomically.
 * <p>
 * Decreasing will not go below minimum value, instead an underflow flag is set.
 * <p>
 * Increasing will not go above maximum value, instead an overflow flag is set.
 * <p>
 * After underflow or overflow flags are set, the increments and decrements are 
 * not taken into account, but the set flag stays until reset.
 * <p>
 * If minimum and maximum values are the same, the minimum value is always
 * returned as next or previous number in sequence without any further 
 * calculations or atomic number swapping. As consequence, if the first 
 * operation is decrease, underflow is set and if the first operation is 
 * increase, overflow is set.
 *
 * @author Florin Ganea
 * @version 1.0
 */
@SuppressWarnings("ClassWithoutLogger")
public class LimitedInt {

    /** Internal reference of atomic integer updater used for atomic swap. */
    private static final AtomicIntegerFieldUpdater<LimitedInt> updater;

    static {
        try {
            updater = AtomicIntegerFieldUpdater.newUpdater(LimitedInt.class, "value");
        } catch (Exception ex) {
            throw new Error(ex);
        }
    }
    /** Next value in sequence. */
    private volatile int value;
    /** Minimum value of the sequence; unchangeable. */
    private final int min;
    /** Maximum value of the sequence; unchangeable. */
    private final int max;
    
    private volatile boolean underflow;
    
    private volatile boolean overflow;

    /**
     * Constructor for the Round-Robin sequence with minimum and maximum values.
     *
     * @param minValue the minimum value of the Round-Robin sequence
     * @param maxValue the maximum value of the Round-Robin sequence
     *
     * @throws IllegalArgumentException if maximum value is less than the
     *                                  minimum value
     */
    public LimitedInt(int minValue, int maxValue)
            throws IllegalArgumentException {
        if (maxValue < minValue) {
            throw new IllegalArgumentException("max value (" + maxValue + ") is less than min value (" + minValue + ")");
        }
        this.min = minValue;
        this.max = maxValue;
        //
        this.value = minValue;
        this.underflow = false;
        this.overflow = false;
    }

    /**
     * Gets the underflow status. 
     * If the decrements reached the minimum value, this flag is set.
     * 
     * @return <code>true</code> if last operation caused underflow
     */
    public boolean hasUnderflow() {
        return this.underflow;
    }
    
    /**
     * Gets the overflow status.
     * If the increments reached the maximum value, this flag is set.
     * 
     * @return <code>true</code> if last operation caused overflow
     */
    public boolean hasOverflow() {
        return this.overflow;
    }
    
    /**
     * Atomically sets the value to the given updated value if the current value
     * {@code ==} the expected value.
     *
     * @param expect the expected value
     * @param update the new value
     *
     * @return true if successful. False return indicates that the actual value
     *         was not equal to the expected value.
     *
     * @see java.util.concurrent.atomic.AtomicIntegerFieldUpdater#compareAndSet(java.lang.Object, int, int)
     */
    private boolean compareAndSet(int expect, int update) {
        return updater.compareAndSet(this, expect, update);
    }

    /**
     * Atomically increments by one the current value taking into account the
     * limits: when {@link #max} is reached, the value is not incremented and 
     * overflow flag is set.
     * <p>
     * If underflow is already set, the min value is returned.
     *
     * @return the previous value
     *
     * @see java.util.concurrent.atomic.AtomicIntegerFieldUpdater#getAndIncrement(java.lang.Object) 
     */
    public int getAndIncrement() {
        int current, next;
        while (true) {
            // read the flags first
            // in case one of the flags was already set, return correspondent value
            if (this.overflow) {
                return this.max;
            }
            if (this.underflow) {
                return this.min;
            }
            // read value after reading the flags:
            // - if a flag set is missed, it will be handled correctly
            // - if value change is missed, will break the implementation
            current = this.value;
            if (current >= this.max) {
                this.overflow = true;
                return this.max;
            } else {
                next = current + 1;
            }
            if (compareAndSet(current, next)) {
                return current;
            }
        }
    }

    /**
     * Atomically decrements by one the current value taking into account the
     * limits: when {@link #min} is reached, the value is not decremented and
     * underflow flag is set.
     * <p>
     * If overflow is already set, max value is returned.
     * 
     * @return the previous value
     *
     * @see java.util.concurrent.atomic.AtomicIntegerFieldUpdater#getAndIncrement(java.lang.Object) 
     */
    public int getAndDecrement() {
        int current, next;
        while (true) {
            // read the flags first
            // in case one of the flags was already set, return correspondent value
            if (this.overflow) {
                return this.max;
            }
            if (this.underflow) {
                return this.min;
            }
            // read value after reading the flags:
            // - if a flag set is missed, it will be handled correctly
            // - if value change is missed, will break the implementation
            current = this.value;
            if (current <= this.min) {
                this.underflow = true;
                return this.min;
            } else {
                next = current - 1;
            }
            if (compareAndSet(current, next)) {
                return current;
            }
        }
    }

    /**
     * Gets the current value.
     * 
     * In case of overflow, maximum value is returned.
     * In case of underflow, minimum value is returned.
     * 
     * @return the current value
     */
    public int get() {
        // this one here is, again almost thread-safe
        // hopefully, if value is correctly set, there will not be inconsistency
        // this is done like this to be consistent with reset
        if (this.underflow) {
            return this.min;
        }
        if (this.overflow) {
            return this.max;
        }
        return this.value;
    }
    
    /**
     * Resets the current value (to minimum value) and the 
     * overflow / underflow flags.
     */
    public void reset() {
        // here is a little trick to make this almost thread-safe
        // - set as underflow to inhibit operations
        // - set the value as min
        // - reset the overflow 
        // - reset the underflow to allow operations
        // note: there might be cases when underflow is incorrectly reported
        //   due this reset procedure
        this.underflow = true;
        this.value = this.min;
        this.overflow = false;
        this.underflow = false; 
    }
    
    /**
     * Returns the string representation of the instance in the form:
     * LimitedInt: {min = &lt;{@link #min}&gt;, max = &lt;{@link #max}&gt;,
     * value = &lt;{@link #value}&gt;, underflow = ..., overflow = ...}
     *
     * @return the string representation of the instance
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(150);
        sb.append(this.getClass().getSimpleName());
        sb.append(": {min = ").append(this.min);
        sb.append(", max = ").append(this.max);
        sb.append(", value = ").append(this.value);
        sb.append(", underflow = ").append(this.underflow);
        sb.append(", overflow = ").append(this.overflow);
        sb.append("}");
        return sb.toString();
    }
}
