/*
 * Copyright (c) 2013, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.atomic;

/**
 * Simple thread-safe sequencer following Round-Robin algorithm for {@code long}.
 * <p>
 * The next number in sequence generation is performed atomically.
 * 
 * @author Florin Ganea
 * @version $Revision: $
 */
@SuppressWarnings("ClassWithoutLogger")
public class SequencerLong implements Sequencer {
    
    /** Minimum value of the sequence; unchangeable. */
    private final long min;
    /** Maximum value of the sequence; unchangeable. */
    private final long max;
    
    /** Next value in sequence. */
    private long value;
    
    /**
     * Constructor for the Round-Robin sequence with minimum and maximum values.
     *
     * @param minValue the minimum value of the Round-Robin sequence
     * @param maxValue the maximum value of the Round-Robin sequence
     *
     * @throws IllegalArgumentException if maximum value is less than the
     *                                  minimum value
     */
    public SequencerLong(long minValue, long maxValue)
            throws IllegalArgumentException {
        if (maxValue < minValue) {
            throw new IllegalArgumentException("max value (" + maxValue + ") is less than min value (" + minValue + ")");
        }
        this.min = minValue;
        this.max = maxValue;
        //
        this.value = this.min;
    }
    
    @Override
    public int getNextInt() {
        throw new UnsupportedOperationException("Sequencer is not allowed to narrow long to int.");
    }
    
    /**
     * Gets the next value in sequence according to the Round-Robin algorithm.
     * <p>
     * First value in sequence is {@link #min}.
     * <p>
     * If the last value returned was {@link #max}, the next value in sequence
     * is {@link #min}.
     *
     * @return the next value in sequence
     */
    @Override
    public long getNextLong() {
        long result;
        
        synchronized (this) {
            result = this.value;
            if (this.value >= this.max) {
                this.value = this.min;
            } else {
                this.value++; 
            }
        }
        
        return result;
    }
    
    /**
     * Returns the string representation of the instance in the form:
     * SequencerLong: {min = &lt;{@link #min}&gt;, max = &lt;{@link #max}&gt;,
     * value = &lt;{@link #value}&gt;}
     *
     * @return the string representation of the instance
     */
    @Override
    public String toString() {
        long lvalue;
        synchronized (this) {
            lvalue = this.value;
        }
        StringBuilder sb = new StringBuilder(150);
        sb.append(this.getClass().getSimpleName());
        sb.append(": {min = ").append(this.min);
        sb.append(", max = ").append(this.max);
        sb.append(", value = ").append(lvalue);
        sb.append("}");
        return sb.toString();
    }

}
