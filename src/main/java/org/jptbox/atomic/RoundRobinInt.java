/*
 * Copyright (c) 2013-2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.atomic;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

/**
 * Non-blocking thread-safe implementation of Round-Robin algorithm for
 * {@code int}.
 * <p>
 * The next number in sequence generation is performed atomically.
 * <p>
 * If minimum and maximum values are the same, the minimum value is always
 * returned as next number in sequence without any further calculations or
 * atomic number swapping.
 *
 * @author Florin Ganea
 * @version $Revision: $
 */
public class RoundRobinInt implements Sequencer {

    /** Internal reference of atomic integer updater used for atomic swap. */
    private static final AtomicIntegerFieldUpdater<RoundRobinInt> updater;

    static {
        try {
            updater = AtomicIntegerFieldUpdater.newUpdater(RoundRobinInt.class, "value");
        } catch (Exception ex) {
            throw new Error(ex);
        }
    }
    /** Next value in sequence. */
    private volatile int value;
    /** Minimum value of the sequence; unchangeable. */
    private final int min;
    /** Maximum value of the sequence; unchangeable. */
    private final int max;

    /**
     * Constructor for the Round-Robin sequence with minimum and maximum values.
     *
     * @param minValue the minimum value of the Round-Robin sequence
     * @param maxValue the maximum value of the Round-Robin sequence
     *
     * @throws IllegalArgumentException if maximum value is less than the
     *                                  minimum value
     */
    public RoundRobinInt(int minValue, int maxValue)
            throws IllegalArgumentException {
        if (maxValue < minValue) {
            throw new IllegalArgumentException("max value (" + maxValue + ") is less than min value (" + minValue + ")");
        }
        this.min = minValue;
        this.max = maxValue;
        //
        this.value = minValue;
    }

    /**
     * Atomically sets the value to the given updated value if the current value
     * {@code ==} the expected value.
     *
     * @param expect the expected value
     * @param update the new value
     *
     * @return true if successful. False return indicates that the actual value
     *         was not equal to the expected value.
     *
     * @see java.util.concurrent.atomic.AtomicIntegerFieldUpdater#compareAndSet(java.lang.Object, int, int)
     */
    private boolean compareAndSet(int expect, int update) {
        return updater.compareAndSet(this, expect, update);
    }

    /**
     * Atomically increments by one the current value taking into account the
     * limits: when {@link #max} is reached, the next value is set to
     * {@link #min}.
     *
     * @return the previous value
     *
     * @see java.util.concurrent.atomic.AtomicIntegerFieldUpdater#getAndIncrement(java.lang.Object) 
     */
    private int getAndIncrement() {
        int current, next;
        while (true) {
            current = this.value;
            if (current >= this.max) {
                next = this.min;
            } else {
                next = current + 1;
            }
            if (compareAndSet(current, next)) {
                return current;
            }
        }
    }

    /**
     * Gets the next value in sequence according to the Round-Robin algorithm.
     * <p>
     * First value in sequence is {@link #min}.
     * <p>
     * If the last value returned was {@link #max}, the next value in sequence
     * is {@link #min}.
     *
     * @return the next value in sequence
     */
    @Override
    public int getNextInt() {
        int result;
        if (this.min == this.max) {
            result = this.min;
        } else {
            result = getAndIncrement();
        }
        return result;
    }
    
    @Override
    public long getNextLong() {
        return getNextInt();
    }

    /**
     * Returns the string representation of the instance in the form:
     * RoundRobinInt: {min = &lt;{@link #min}&gt;, max = &lt;{@link #max}&gt;,
     * value = &lt;{@link #value}&gt;}
     *
     * @return the string representation of the instance
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(150);
        sb.append(this.getClass().getSimpleName());
        sb.append(": {min = ").append(this.min);
        sb.append(", max = ").append(this.max);
        sb.append(", value = ").append(this.value);
        sb.append("}");
        return sb.toString();
    }
}
