/*
 * Copyright (c) 2013-2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.tlv;

/**
 * The TLV types that are recognized by encoders and decoders.
 * <p>
 * <b>Recommendation:</b> do not insert enum values, but only add them at 
 * the end of the list if there is a requirement to keep compatibility
 * with encodings produced by a former version.
 *
 * @author Florin Ganea
 * @version 1.0
 */
public enum Type {
    
    /**
     * The boolean data type.
     * It has two values: true and false.
     */
    BOOLEAN,

    /**
     * The bit data type is just a bit.
     * It has two values: 0 and 1.
     */
    BIT,

    /**
     * The char data type is a single 16-bit Unicode character.
     * It has a minimum value of '\u0000' (or 0) and a maximum value of '\uffff' (or 65,535 inclusive).
     */
    CHAR,

    /**
     * The byte data type is an 8-bit signed two's complement integer.
     * It has a minimum value of -128 and a maximum value of 127 (inclusive).
     */
    BYTE,

    /**
     * The short data type is a 16-bit signed two's complement integer.
     * It has a minimum value of -32,768 and a maximum value of 32,767 (inclusive).
     */
    SHORT,

    /**
     * The int data type is a 32-bit signed two's complement integer.
     * It has a minimum value of -2<sup>31</sup> and a maximum value of 2<sup>31</sup>-1 (inclusive).
     */
    INT,

    /**
     * The long data type is a 64-bit two's complement integer.
     * The signed long has a minimum value of -2<sup>63</sup> and a maximum value of 2<sup>63</sup>-1.
     */
    LONG,

    /**
     * The float data type is a single-precision 32-bit IEEE 754 floating point.
     */
    FLOAT,

    /**
     * The double data type is a double-precision 64-bit IEEE 754 floating point.
     */
    DOUBLE,

    /**
     * String type.
     * Actually it is an array of char.
     */
    STRING,

    /**
     * Array type.
     * The elements of the array are required to have the same type.
     */
    ARRAY,

    /**
     * Message type.
     * Contains a list of TLV-s. It does not have the "same type" requirement as the array type.
     */
    MESSAGE;

    /**
     * Returns the enum constant of this type with the specified ordinal (index as defined).
     * 
     * @param ordinal the defined index of the enum constant
     * 
     * @return the enum constant with the specified ordinal
     * 
     * @throws IllegalArgumentException if this enum type has no constant with the specified ordinal
     */
    public static Type valueOf(int ordinal) throws IllegalArgumentException {
        Type[] values = Type.values();
        if (ordinal < 0 || ordinal >= values.length) {
            throw new IllegalArgumentException("ordinal not in expected range: 0 .. " + String.valueOf(values.length - 1));
        }
        return Type.values()[ordinal];
    }

}
