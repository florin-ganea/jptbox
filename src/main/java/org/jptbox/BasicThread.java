/*
 * Copyright (c) 2013-2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox;

import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jptbox.atomic.Sequencer;
import org.jptbox.atomic.SequencerInt;

/**
 * Base class for threads.
 * <p>
 * The thread can be stopped at any time by calling {@link #shutdown()} method.
 * <p>
 * The thread status is set as {@link ThreadStatus#NEW} at object creation and 
 * can only be changed as following:
 * <ul>
 * <li>from {@link ThreadStatus#NEW} to {@link ThreadStatus#INITIALIZING} (run method, triggered by thread start)
 * <li>from {@link ThreadStatus#NEW} to {@link ThreadStatus#TERMINATED} (shutdown)
 * <li>from {@link ThreadStatus#INITIALIZING} to {@link ThreadStatus#RUNNING} (run)
 * <li>from {@link ThreadStatus#INITIALIZING} to {@link ThreadStatus#TERMINATING} (shutdown)
 * <li>from {@link ThreadStatus#RUNNING} to {@link ThreadStatus#TERMINATING} (shutdown, run)
 * <li>from {@link ThreadStatus#TERMINATING} to {@link ThreadStatus#TERMINATED} (run)
 * </ul>
 *
 * @author Florin Ganea
 * @version 1.0
 */
public abstract class BasicThread extends Thread {

    /** Internal logger reference. */
    private static final Logger LOG = Logger.getLogger(BasicThread.class.getPackage().getName());
    /** The default thread name prefix. */
    public static final String DEFAULT_THREAD_NAME = "JPTh";
    /**
     * Maximum thread index to be used in thread names.
     * <p>
     * When reached, the numbering restarts from 0.
     * <p>
     * Actual thread index is never set to this value.
     */
    private static final int MAX_THREAD_INDEX = Integer.MAX_VALUE;
    /** Thread index used in automatic naming. */
    private static final Sequencer threadIndex;

    static {
        threadIndex = new SequencerInt(0, MAX_THREAD_INDEX);
    }

    /**
     * Generates a new thread index to be used in thread naming.
     * <p>
     * The indexes are generated sequential, round-robin in interval [0 ..
     * {@link #MAX_THREAD_INDEX}].
     *
     * @return a new sequential thread index
     */
    protected static int nextThreadIndex() {
        return threadIndex.getNextInt();
    }
    
    /** Thread status; one of {@link ThreadStatus} values. */
    private final AtomicReference<ThreadStatus> status;
    
    /** Monitor used to wait the thread to enter {@link ThreadStatus#RUNNING} 
     * status and to inform about this event. */
    private final Object startMonitor;

    {
        this.status = new AtomicReference<>(ThreadStatus.NEW);
        this.startMonitor = new Object();
    }

    /**
     * Constructs a new thread instance with default name.
     * <p>
     * The default thread name is {@link #DEFAULT_THREAD_NAME} + generated thread index.
     */
    public BasicThread() {
        this(DEFAULT_THREAD_NAME);
    }

    /**
     * Constructs a new thread instance with the provided name.
     * <p>
     * The actual thread name is formed by the provided name + "-" + generated
     * thread index.
     *
     * @param name the thread name
     */
    public BasicThread(String name) {
        super(name + "-" + nextThreadIndex());
    }

    /**
     * Implementation of the {@link Thread#run()} method.
     * <p>
     * The thread running steps are the following:
     * <ol>
     * <li>enter the {@link ThreadStatus#INITIALIZING} status</li>
     * <li>call the {@link #initialize()} method</li>
     * <li>enter the {@link ThreadStatus#RUNNING} status</li>
     * <li>call the {@link #execute()} method</li>
     * <li>enter the {@link ThreadStatus#TERMINATING} status</li>
     * <li>call the {@link #terminate()} method</li>
     * <li>enter the {@link ThreadStatus#TERMINATED} status</li>
     * </ol>
     */
    @Override
    public final void run() {
        boolean shouldContinue;
        
        LOG.log(Level.SEVERE, "thread starting: {0}", this.getName());
        
        // check if the thread status is still new and move the status to initializing
        // if the thread is not new anymore, do not attempt to execute anything
        shouldContinue = 
                this.status.compareAndSet(ThreadStatus.NEW, ThreadStatus.INITIALIZING);

        if (shouldContinue) {
            // initializing...
            try {
                this.initialize();
            }
            catch (InterruptedException ex) {
                LOG.log(Level.INFO, "thread interrupted (initialize): {0}", this.getName());
                shouldContinue = false;
            }
        }

        // if initializing status was not changed by other means (for example, shutdown)
        // mark as running and notify thread started
        shouldContinue = shouldContinue && 
                this.status.compareAndSet(ThreadStatus.INITIALIZING, ThreadStatus.RUNNING);
            
        // the thread status is not new nor initializing in this point
        // this notification must not be conditioned by anything
        synchronized (this.startMonitor) {
            this.startMonitor.notifyAll();
        }

        if (shouldContinue) {
            LOG.log(Level.SEVERE, "thread started: {0}", this.getName());
            // executing...
            try {
                this.execute();
            }
            catch (InterruptedException ex) {
                LOG.log(Level.INFO, "thread interrupted (execute): {0}", this.getName());
            }

            // try to change from running to terminating
            if (!this.status.compareAndSet(ThreadStatus.RUNNING, ThreadStatus.TERMINATING)) {
                // probably stopped intentionally before natural end
                LOG.log(Level.SEVERE, "thread interrupted: {0}", this.getName());
            }
        }

        // only thing that matters now is whether the current state is terminating
        // in such case, the terminate method should be called and the state
        // moved to terminated
        if (this.status.get() == ThreadStatus.TERMINATING) {
            LOG.log(Level.SEVERE, "thread stopping: {0}", this.getName());
            try {
                this.terminate();
            }
            catch (InterruptedException ex) {
                LOG.log(Level.INFO, "thread interrupted (terminate): {0}", this.getName());
            }
            this.status.set(ThreadStatus.TERMINATED);
        }

        LOG.log(Level.SEVERE, "thread stopped: {0}", this.getName());        
    }

    /**
     * This method is called by the thread before entering in the 
     * {@link ThreadStatus#RUNNING} status.
     * 
     * @throws InterruptedException
     * @see #run() 
     */
    public abstract void initialize() throws InterruptedException;
    
    /**
     * This method is called by the thread after leaving the 
     * {@link ThreadStatus#RUNNING} status.
     * 
     * @throws InterruptedException
     * @see #run() 
     */
    public abstract void terminate() throws InterruptedException;
    
    /**
     * This method implements the actions performed by the thread while in 
     * {@link ThreadStatus#RUNNING} status.
     * <p>
     * The implementation of this method MUST check the thread status using
     * {@link #getStatus()} method and make sure that the execution is 
     * terminated (or not even started) when the thread status is not 
     * {@link ThreadStatus#RUNNING}.
     * 
     * @throws InterruptedException
     * @see #run() 
     */
    public abstract void execute() throws InterruptedException;
    
    /**
     * Waits for the thread to be started and enter in the running status.
     * <p>
     * Calling this method on an already terminated thread has no effect
     * (immediately returns).
     */
    public final void waitRunning() {
        ThreadStatus lstatus = this.status.get();
        while (lstatus == ThreadStatus.NEW
                || lstatus == ThreadStatus.INITIALIZING) {
            try {
                synchronized (this.startMonitor) {
                    this.startMonitor.wait();
                }
            } catch (InterruptedException ex) {
                LOG.log(Level.INFO, "interrupted", ex);
            }
            // get the status when exiting the wait
            lstatus = this.status.get();
        }
    }

    /**
     * Waits for the thread to be stopped for a specified amount of time.
     * <p>
     * Calling this method on a not started or an already terminated thread has
     * no effect (immediately returns).
     * <p>
     * Calling this method from the self thread does not have any effect 
     * (immediately returns).
     * 
     * @todo Handle spurious wake-ups, join is not quite ok since it can be
     * interrupted by legitimate shutdown etc.
     * 
     * @param timeout the time to wait for the thread to terminate; 0 means 
     *                without time constraint (wait forever)
     */
    public final void waitTerminated(long timeout) {
        // do not join self thread :-)
        if (Thread.currentThread() != this) {
            try {
                this.join(timeout);
            } catch (InterruptedException ex) {
                LOG.log(Level.INFO, "interrupted", ex);
            }
        }
    }

    /**
     * Gets the thread status.
     * 
     * @return the thread status
     */
    protected final ThreadStatus getStatus() {
        return this.status.get();
    }
    
    /**
     * Gets the running status of the thread.
     * <p>
     * This status is a combination of internal running flag and the standard
     * alive flag as provided by {@link Thread#isAlive()} method.
     *
     * @return the running status of the thread
     */
    public final boolean isRunning() {
        return this.status.get() == ThreadStatus.RUNNING && this.isAlive();
    }

    /**
     * Stops the thread.
     * <p>
     * This method sets the internal running flag to false and interrupts the
     * thread to make sure that it does not remain blocked in some operations
     * (like I/O).
     * <p>
     * If the thread is not running, this method may not have any effect.
     * <p>
     * This method subsequently calls {@link Thread#interrupt()}.
     */
    public final void shutdown() {
        // the thread can be: new, initializing, running, terminating or terminated
        // the thread can only be moved from lower status to higher status
        
        // if not yet run, then just mark it as terminated
        if (this.status.compareAndSet(ThreadStatus.NEW, ThreadStatus.TERMINATED)) {
            // nothing to do
            // the thread did not manage to actually start
        }
        
        // move from initializing to terminating and interrupt the thread
        if (this.status.compareAndSet(ThreadStatus.INITIALIZING, ThreadStatus.TERMINATING)) {
            this.interrupt();
        }

        // move from running to terminating and interrupt the thread
        if (this.status.compareAndSet(ThreadStatus.RUNNING, ThreadStatus.TERMINATING)) {
            this.interrupt();
        }

        // in this moment, the thread can be:
        // - terminating
        // - terminated
    }
}
