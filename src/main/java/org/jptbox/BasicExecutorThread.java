/*
 * Copyright (c) 2014, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox;

import java.util.concurrent.BlockingQueue;

/**
 * Simple thread that executes runnable objects taken from a blocking queue.
 *
 * @author Florin Ganea
 * @version 1.0
 * 
 * @param <T> the type of the objects that are executed; subtype of {@link Runnable}
 */
public class BasicExecutorThread<T extends Runnable> extends IterativeThread {

    /** the queue of tasks */
    private final BlockingQueue<T> queue;
    
    /**
     * Creates a thread that will execute runnable objects from the given queue.
     * 
     * @param queue the queue containing the runnable objects to be executed
     * 
     * @throws IllegalArgumentException if the queue is null
     */
    public BasicExecutorThread(BlockingQueue<T> queue) {
        super();
        if (queue == null) {
            throw new IllegalArgumentException("null queue");
        }
        this.queue = queue;
    }

    /**
     * Takes one object from the queue and executes it.
     * <p>
     * If the queue is empty, it blocks waiting for some element to be available.
     * 
     * @throws InterruptedException when waiting for some element to be available 
     *                              is interrupted
     */
    @Override
    public void runIteration() throws InterruptedException {
        T task = this.queue.take();
        if (task != null) {
            task.run();
        }
    }
    
}
