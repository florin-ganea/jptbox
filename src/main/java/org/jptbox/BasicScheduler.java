/*
 * Copyright (c) 2014-2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Simple extension of the {@link ScheduledThreadPoolExecutor}.
 * <p>
 * It has no special purpose yet and probably will be removed in the future.
 * 
 * @author Florin Ganea
 */
public class BasicScheduler extends ScheduledThreadPoolExecutor {
    
    /**
     * Constructor with name.
     * 
     * The new scheduler has one thread created by {@link RunnableThreadFactory}.
     * 
     * @param name the scheduler name
     */
    public BasicScheduler(String name) {
        super(1, new RunnableThreadFactory(name));
    }
    
    /**
     * Schedules an one time execution of a task with the initial specified 
     * delay.
     * 
     * @param task  the task to be executed
     * @param delay the delay in milliseconds
     * 
     * @return a ScheduledFuture representing pending completion of the task 
     *         and whose {@code get()} method will return {@code null} upon 
     *         completion
     * 
     * @throws RejectedExecutionException if the task cannot be scheduled for 
     *                                    execution
     * @throws NullPointerException if command is null
     */
    public ScheduledFuture<?> schedule(Runnable task, long delay) {
        return super.schedule(task, delay, TimeUnit.MILLISECONDS);
    }
    
    /**
     * Schedules a recurrent task execution with the initial delay and a 
     * specified period, having a fixed rate.
     *
     * @param task      the task to be executed
     * @param delay     the initial delay in milliseconds
     * @param period    the period between execution starts in milliseconds
     * 
     * @return a ScheduledFuture representing pending completion of the task, 
     *         and whose {@code get()} method will throw an exception upon 
     *         cancellation
     * 
     * @throws RejectedExecutionException if the task cannot be scheduled for 
     *                                    execution
     * @throws NullPointerException if command is null
     * @throws IllegalArgumentException if period less than or equal to zero
     */
    public ScheduledFuture<?> schedule(Runnable task, long delay, long period) {
        return super.scheduleAtFixedRate(task, delay, period, TimeUnit.MILLISECONDS);
    }
    
}
