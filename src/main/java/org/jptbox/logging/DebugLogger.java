/*
 * Copyright (c) 2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jptbox.logging;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Debugging utility.
 * 
 * @author Florin Ganea
 */
public class DebugLogger {
   
    private static final Map<String, DebugLogger> loggers = new ConcurrentHashMap<>();
    
    /**
     *
     */
    public static final String DEFAULT_LOGGER = "debug";

    /**
     *
     */
    public static final String LOGGER_FQDN = DebugLogger.class.getName();
    
    static {
        loggers.put(DEFAULT_LOGGER, new DebugLogger(DEFAULT_LOGGER));
    }
    
    /**
     *
     * @return
     */
    public static DebugLogger getInstance() {
        return loggers.get(DEFAULT_LOGGER);
    }
    
    /**
     *
     * @param loggerName
     * @return
     */
    public static DebugLogger getInstance(String loggerName) {
        DebugLogger logger = loggers.get(loggerName);
        if (logger == null) {
            logger = new DebugLogger(loggerName);
            DebugLogger prev = loggers.putIfAbsent(loggerName, logger);
            if (prev != null) {
                logger = prev;
            }
        }
        return logger;
    }
    
    private final Logger logger;
    
    private DebugLogger(String loggerName) {
        this.logger = Logger.getLogger(loggerName);
    }
    
    /**
     *
     * @param level
     * @param format
     * @param args
     */
    public void logf(Level level, String format, Object... args) {
        this.logger.log(level, () -> {
                String newFormat = format;
                StackTraceElement[] aste = new Exception().getStackTrace();
                if (aste.length > 3) {
                    StackTraceElement ste = aste[3];
                    newFormat = String.format("(%s: %d) %s", ste.getFileName(), ste.getLineNumber(), format);
                }
                return String.format(newFormat, args);
            });
    }

    /**
     *
     * @param level
     * @param message
     * @param thrown
     */
    public void log(Level level, String message, Throwable thrown) {
        this.logger.log(level, message, thrown);
    }
    
}
