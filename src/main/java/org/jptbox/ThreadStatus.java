/*
 * Copyright (c) 2013-2014, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox;

import java.lang.Thread.State;

/**
 * The thread status from functional point of view.
 * <p>
 * On thread creation the status is {@link #NEW}. It is somehow equivalent with
 * {@link State#NEW} but it is changed to the next status after it.
 * <p>
 * When {@link Thread#run()} method is called, the status is set to {@link #INITIALIZING}.
 * <p>
 * When the initialization part is finished, the status is set to {@link #RUNNING}. 
 * Typically in this status the thread performs a series of actions.
 * <p>
 * When the actions are done, the thread enters the {@link #TERMINATING} status.
 * <p>
 * When the terminating phase is over, the thread enters the {@link #TERMINATED} status.
 * It is somehow equivalent to {@link State#TERMINATED} but it is set before it.
 * 
 * @author Florin Ganea
 * @version 1.0
 */
public enum ThreadStatus {
    /** the thread has been created, but not yet started */
    NEW,
    
    /** the thread has been started and it is initializing */
    INITIALIZING,
    
    /** the thread has been started and initialized and it is performing actions */
    RUNNING,
    
    /** the thread entered in the termination phase */
    TERMINATING,
    
    /** the thread has been terminated */
    TERMINATED
}
