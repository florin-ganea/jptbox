/*
 * Copyright (c) 2014-2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox;

/**
 * Simple thread for executing {@link Runnable} objects.
 * <p>
 * It is based on the {@link BasicThread} and has the same naming scheme and
 * processing flow.
 * <p>
 * This class may be extended, but the {@link #execute()} method shall not
 * be overridden.
 * 
 * @author Florin Ganea
 */
public class RunnableThread extends BasicThread {
    
    /** internal reference of the object to be run */
    private final Runnable target;
    
    /**
     * Creates a new thread for executing the provided {@link Runnable} object.
     * 
     * @param r the object to be executed when the thread is started
     */
    public RunnableThread(Runnable r) {
        super();
        this.target = r;
    }
    
    /**
     * Constructs a new thread with the given name, followed by some index as
     * actual thread name, and with a runnable to execute.
     * 
     * @param name  the thread name (prefix)
     * @param r     the runnable to execute
     */
    public RunnableThread(String name, Runnable r) {
        super(name);
        this.target = r;
    }

    @Override
    public void initialize() throws InterruptedException {
        // noop
    }

    @Override
    public void terminate() throws InterruptedException {
        // noop
    }

    @Override
    public final void execute() throws InterruptedException {
        if (this.target != null && this.isRunning()) {
            this.target.run();
        }
    }
    
}
