/*
 * Copyright (c) 2013-2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jptbox.statemachine.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A state has a name and a definition body
 * &lt;state&gt;      :== &lt;state-name&gt; '{' &lt;state-body&gt; '}'
 * &lt;state-name&gt; :== &lt;identifier&gt;
 *
 * # state definition body has one or more parameters
 * &lt;state-body&gt;      :== (&lt;state-parameter&gt;)+
 * &lt;state-parameter&gt; :==
 *         "init"  |
 *         "final" |
 *         "onentry"   '{' &lt;action-list&gt; '}' |
 *         "onexit"    '{' &lt;action-list&gt; '}' |
 *         "onevent"   '{' &lt;transition-list&gt; '}' |
 *         "ontimeout" '{' &lt;timeout&gt; &lt;transition-destination&gt; '}' |
 *         "ignores"   '{' &lt;event-list&gt; '}'
 *
 * &lt;action-list&gt; :== (&lt;action&gt; (',' &lt;action&gt;)*)?
 * &lt;event-list&gt;  :== (&lt;event&gt; (',' &lt;event&gt;)*)?
 * &lt;transition-list&gt; :== (&lt;transition&gt;)*
 */
public class StateParser implements Parser {
    
    private static final Logger LOG = Logger.getLogger(StateParser.class.getPackage().getName());
    
    /**
     *
     */
    public final IdentifierParser name;

    /**
     *
     */
    public final AtomicBoolean isInit;

    /**
     *
     */
    public final AtomicBoolean isFinal;

    /**
     *
     */
    public final ArrayList<ActionParser> onEntry;

    /**
     *
     */
    public final ArrayList<ActionParser> onExit;

    /**
     *
     */
    public final ArrayList<TransitionParser> onEvent;

    /**
     *
     */
    public final ArrayList<EventParser> ignores;

    /**
     *
     */
    public final AtomicLong timeout;

    /**
     *
     */
    public final TransitionDestinationParser onTimeout;
    
    {
        this.name = new IdentifierParser();
        this.isInit = new AtomicBoolean(false);
        this.isFinal = new AtomicBoolean(false);
        this.onEntry = new ArrayList<>(2);
        this.onExit = new ArrayList<>(2);
        this.onEvent = new ArrayList<>(8);
        this.ignores = new ArrayList<>(4);
        this.timeout = new AtomicLong(0);
        this.onTimeout = new TransitionDestinationParser();
    }

    @Override
    public boolean parse(StateMachineStream reader) throws IOException {
        boolean done = false;
        boolean result;
        result = name.parse(reader);
        if (!result) {
            if (!reader.isEof()) {
                LOG.log(Level.WARNING, "failed to parse state name; not an identifier");
                reader.trace(this, result);
            }
            return result;
        }
        else {
            LOG.log(Level.FINEST, "parse state name ok: {0}", name);
        }
        TokenParser t = new TokenParser();
        do {
            result = t.parse(reader);
            if (result) {
                LOG.log(Level.FINEST, "got token: {0}", t);
                // got a token
                switch (t.value) {
                    case "{":
                        // begin definition
                        break;
                    case "}":
                        // end definition
                        done = true;
                        break;
                    case "init":
                        isInit.set(true);
                        break;
                    case "final":
                        isFinal.set(true);
                        break;
                    case "onentry":
                        // parse action list...
                        result = ListItemParser.parseList(reader, onEntry, ActionParser.class);
                        break;
                    case "onexit":
                        // parse action list...
                        result = ListItemParser.parseList(reader, onExit, ActionParser.class);
                        break;
                    case "onevent":
                        // parse transition list...
                        result = ListItemParser.parseList(reader, onEvent, TransitionParser.class);
                        break;
                    case "ontimeout":
                        // parse special transition...
                        // {
                        result = t.parse(reader);
                        if (result) {
                            if (t.value.equals("{")) {
                                // timeout
                                result = t.parse(reader);
                                if (result) {
                                    try {
                                        timeout.set(Long.parseLong(t.value));
                                    }
                                    catch (NumberFormatException ex) {
                                        result = false;
                                    }
                                }
                            }
                            else {
                                // unexpected... should be {
                                result = false;
                            }
                        }
                        if (result) {
                            // transition destination in case of timeout
                            result = this.onTimeout.parse(reader);
                        }
                        // }
                        if (result) {
                            result = t.parse(reader);
                            if (result) {
                                result = t.value.equals("}");
                            }
                        }
                        break;
                    case "ignores":
                        // parse event list...
                        result = ListItemParser.parseList(reader, ignores, EventParser.class);
                        break;
                    default:
                        // error
                        result = false;
                }
            }
        }
        while (result && !done);
        
        result = result && done;
        //
        reader.trace(this, result);
        //
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("State {name = ").append(name);
        sb.append(", isInit = ").append(isInit);
        sb.append(", isFinal = ").append(isFinal);
        sb.append(", onEntry = ").append(onEntry);
        sb.append(", onExit = ").append(onExit);
        sb.append(", onEvent = ").append(onEvent);
        sb.append(", ignores = ").append(ignores);
        sb.append(", timeout = ").append(timeout);
        sb.append(", onTimeout = ").append(onTimeout);
        sb.append("}");
        return sb.toString();
    }
    
}
