/*
 * Copyright (c) 2013, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.statemachine.config;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

/**
 *
 * @author Florin Ganea
 */
public class StateMachineStream {

    private final Reader reader;    
    private volatile boolean eof;
    
    private volatile Tracer tracer;    
    private volatile Parser lastSuccessful;
    
    {
        this.eof = false;
        this.tracer = null;
        this.lastSuccessful = null;        
    }
    
    /**
     *
     * @param reader
     */
    public StateMachineStream(Reader reader) {
        this.reader = reader;
    }
    
    /**
     *
     * @param fileName
     * @throws FileNotFoundException
     */
    public StateMachineStream(String fileName) throws FileNotFoundException {
        this(new BufferedReader(new FileReader(fileName)));
    }
    
    /**
     *
     * @return
     * @throws IOException
     */
    public char read() throws IOException {
        int c = this.reader.read();
        if (c < 0) {
            eof = true;
        }
        return (char) c;
    }
    
    /**
     *
     * @param readAheadLimit
     * @throws IOException
     */
    public void mark(int readAheadLimit) throws IOException {
        this.reader.mark(readAheadLimit);
    }
    
    /**
     *
     * @throws IOException
     */
    public void reset() throws IOException {
        this.reader.reset();
    }
    
    /**
     *
     * @param token
     * @param success
     */
    public void trace(Parser token, boolean success) {
        if (success) {
            lastSuccessful = token;
        }
        Tracer t = tracer;
        if (t != null) {
            t.parsed(token.toString(), success);
        }
    }
    
    /**
     *
     * @param tracer
     */
    public void setTracer(Tracer tracer) {
        this.tracer = tracer;
    }
    
    /**
     *
     * @return
     */
    public Parser getLastSuccessful() {
        return this.lastSuccessful;
    }
    
    /**
     *
     * @return
     */
    public boolean isEof() {
        return this.eof;
    }

}
