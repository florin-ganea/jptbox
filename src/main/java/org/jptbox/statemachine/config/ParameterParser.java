/*
 * Copyright (c) 2013-2014, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.statemachine.config;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Parser for state-machine Event or Action Parameter.
 * <p>
 * &lt;parameter&gt; :== &lt;identifier&gt; ('=' &lt;value&gt;)?
 */
public class ParameterParser extends ListItemParser {
    
    private static final Logger LOG = Logger.getLogger(ParameterParser.class.getPackage().getName());

    /**
     *
     */
    public final IdentifierParser name;

    /**
     *
     */
    public volatile String value;

    {
        this.name = new IdentifierParser(true);
        this.value = null;
    }

    @Override
    public boolean parse(StateMachineStream reader) throws IOException {
        boolean result;
        boolean hasValue = false;
        // read the name
        result = name.parse(reader);
        // read the = sign
        if (result) {
            TokenParser t = new TokenParser();
            result = t.parse(reader);
            if (result) {
                switch (t.value) {
                    case "=":
                        // value follows
                        hasValue = true;
                        break;
                    case ";":
                        // value is empty
                        hasValue = false;
                        break;
                    case ")":
                        // end of parameter list
                        hasValue = false;
                        this.isLast.set(true);
                        break;
                    default:
                        // unexpected
                        result = false;
                }
            }
        }
        // read the value
        if (result && hasValue) {
            boolean quote = false;
            boolean escape = false;
            StringBuilder sb = new StringBuilder(32);
            char c;
            boolean done = false;
            // search for ';' or for ')' (or for whitespace?)
            do {
                c = reader.read();
                // check eof
                if (reader.isEof()) {
                    LOG.log(Level.WARNING, "end of stream reached");
                    done = true;
                }
                else // check for quote
                if (quote) {
                    if (escape) {
                        escape = false;
                        sb.append(c);
                    }
                    else {
                        if (c == '\\') {
                            escape = true;
                        }
                        else {
                            if (c == '"') {
                                quote = false;
                            }
                            sb.append(c);
                        }
                    }
                }
                else if (c == '"') {
                    quote = true;
                    sb.append(c);
                }
                else if (c == ';' || c == ')') {
                    done = true;
                    if (c == ')') {
                        isLast.set(true);
                    }
                }
                else {
                    sb.append(c);
                }
            }
            while (!done);
            value = sb.toString().trim();
            result = (!reader.isEof());
        }
        //
        reader.trace(this, result);
        //
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(32);
        sb.append("Parameter {name = ").append(name).append(", value = \"").append(value).append("\"}");
        return sb.toString();
    }
    
}
