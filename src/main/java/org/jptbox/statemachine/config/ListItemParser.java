/*
 * Copyright (c) 2013, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.statemachine.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A list item is the base class for items that can be part of lists.
 * <p>
 * The purpose is to have a flag saying whether the item is the last in the list.
 */
public abstract class ListItemParser implements Parser {
    
    private static final Logger LOG = Logger.getLogger(ListItemParser.class.getPackage().getName());

    /**
     *
     */
    public final AtomicBoolean isLast;

    {
        this.isLast = new AtomicBoolean(false);
    }

    static <T extends ListItemParser> boolean parseList(StateMachineStream reader, ArrayList<T> list, Class<T> clazz) throws IOException {
        boolean result;
        TokenParser t;
        t = new TokenParser();
        result = t.parse(reader);
        if (result) {
            if (t.value.equals("{")) {
                // begin list
                boolean inList = true;
                while (result && inList) {
                    try {
                        T o = clazz.newInstance();
                        result = o.parse(reader);
                        if (result) {
                            list.add(o);
                            inList = !o.isLast.get();
                        }
                        else {
                            inList = false;
                            result = o.isLast.get();
                        }
                    }
                    catch (InstantiationException | IllegalAccessException ex) {
                        result = false;
                        LOG.log(Level.WARNING, "new instance exception", ex);
                    }
                }
            }
            else {
                LOG.log(Level.WARNING, "unexpected token: {0}", t.value);
                result = false;
            }
        }
        else {
            LOG.log(Level.WARNING, "could not parse token");
        }
        return result;
    }
    
}
