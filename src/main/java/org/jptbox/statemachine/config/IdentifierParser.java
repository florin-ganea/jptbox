/*
 * Copyright (c) 2013-2014, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.statemachine.config;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Parser for an Identifier (not state-machine specific).
 * <p>
 * &lt;identifier&gt; :== &lt;letter&gt; (&lt;letter&gt; | &lt;digit&gt; | '_')*
 */
public class IdentifierParser implements Parser {
    
    private static final Logger LOG = Logger.getLogger(IdentifierParser.class.getPackage().getName());
    
    /**
     *
     */
    public volatile String value;
    
    private final boolean allowDash;
    
    {
        this.value = "";
    }

    /**
     *
     * @param allowDash
     */
    public IdentifierParser(boolean allowDash) {
        this.allowDash = allowDash;
    }

    /**
     *
     */
    public IdentifierParser() {
        this(false);
    }

    @Override
    public boolean parse(StateMachineStream reader) throws IOException {
        char c;
        boolean done = false;
        boolean comment = false;
        StringBuilder sb = new StringBuilder(32);
        boolean result;
        //
        do {
            reader.mark(1);
            c = reader.read();
            if (sb.length() == 0) {
                // before reading actual chars
                if (reader.isEof()) {
                    // eof
                    done = true;
                }
                else if (comment && !StateMachineReader.isEol(c)) {
                }
                else if (comment && StateMachineReader.isEol(c)) {
                    // end of comment
                    comment = false;
                }
                else if (c == '#') {
                    // comment begin
                    comment = true;
                }
                else if (StateMachineReader.isSeparator(c)) {
                }
                else {
                    if (Character.isLetter(c)) {
                        sb.append(c);
                    }
                    else {
                        // bad; revert read char
                        reader.reset();
                        done = true;
                    }
                }
            }
            else {
                // actual chars
                if (Character.isLetterOrDigit(c) || c == '_' || (allowDash && c == '-')) {
                    sb.append(c);
                }
                else {
                    // end of identifier; revert read char
                    reader.reset();
                    done = true;
                }
            }
        }
        while (!done);
        //
        result = sb.length() > 0;
        if (result) {
            value = sb.toString();
        }
        // return true if something meaningful was read
        reader.trace(this, result);
        //
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(32);
        sb.append("Identifier {value = \"").append(value).append("\"}");
        return sb.toString();
    }
    
}
