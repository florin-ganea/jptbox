/*
 * Copyright (c) 2013-2014, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.statemachine.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * <transition-destination> :== <state-name> '{' <action-list> '}'
 */
public class TransitionDestinationParser implements Parser {
    
    private static final Logger LOG = Logger.getLogger(TransitionDestinationParser.class.getPackage().getName());
    
    /**
     *
     */
    public final IdentifierParser stateName;

    /**
     *
     */
    public final ArrayList<ActionParser> actionList;
    
    {
        this.stateName = new IdentifierParser();
        this.actionList = new ArrayList<>(8);
    }

    @Override
    public boolean parse(StateMachineStream reader) throws IOException {
        boolean result;
        result = this.stateName.parse(reader);
        if (result) {
            result = ListItemParser.parseList(reader, actionList, ActionParser.class);
        }
        //
        reader.trace(this, result);
        //
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(32);
        sb.append("TransitionDestination {stateName = ").append(stateName).append(", actionList = ").append(actionList).append("}");
        return sb.toString();
    }
    
}
