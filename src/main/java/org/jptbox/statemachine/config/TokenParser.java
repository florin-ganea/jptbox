/*
 * Copyright (c) 2013, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.statemachine.config;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Generic token.
 * It skips:
 * - whitespace
 * - comments
 *
 * It ends on:
 * - whitespace
 * - separator: ,;{}()=
 */
public class TokenParser implements Parser {
    
    private static final Logger LOG = Logger.getLogger(TokenParser.class.getPackage().getName());
    
    /**
     *
     */
    public volatile String value;
    
    {
        this.value = "";
    }

    @Override
    public boolean parse(StateMachineStream reader) throws IOException {
        int c;
        boolean done = false;
        boolean comment = false;
        boolean result;
        StringBuilder sb = new StringBuilder(32);
        do {
            c = reader.read();
            if (sb.length() == 0) {
                // before reading actual chars
                if (c < 0) {
                    // eof
                    done = true;
                }
                else if (comment && !StateMachineReader.isEol((char) c)) {
                }
                else if (comment && StateMachineReader.isEol((char) c)) {
                    // end of comment
                    comment = false;
                }
                else if (c == '#') {
                    // comment begin
                    comment = true;
                }
                else if (StateMachineReader.isSeparator((char) c)) {
                }
                else {
                    sb.append((char) c);
                    if (c == ',' || c == ';' || c == '{' || c == '}' || c == '(' || c == ')' || c == '=') {
                        done = true;
                    }
                }
            }
            else {
                if (c < 0) {
                    // eof
                    LOG.log(Level.WARNING, "end of stream reached");
                    done = true;
                }
                else // actual chars
                if (StateMachineReader.isSeparator((char) c)) {
                    // separator, not interesting to keep
                    done = true;
                }
                else if (c == ',' || c == ';' || c == '{' || c == '}' || c == '(' || c == ')' || c == '=') {
                    // TODO should this be put back?
                    done = true;
                }
                else {
                    sb.append((char) c);
                }
            }
        }
        while (!done);
        //
        result = sb.length() > 0;
        if (result) {
            value = sb.toString();
        }
        // return true if something meaningful was read
        reader.trace(this, result);
        //
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(32);
        sb.append("Token {value = \"").append(value).append("\"}");
        return sb.toString();
    }
    
}
