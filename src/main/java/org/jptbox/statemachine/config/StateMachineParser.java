/*
 * Copyright (c) 2013-2014, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.statemachine.config;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Parser for the state-machine definition.
 * <p>
 * &lt;state-machine&gt; :== (&lt;state&gt;)+
 */
public class StateMachineParser implements Parser {
    
    private static final Logger LOG = Logger.getLogger(StateMachineParser.class.getPackage().getName());
    
    /**
     *
     */
    public final ArrayList<StateParser> states;
    {
        states = new ArrayList<>(64);
    }

    @Override
    public boolean parse(StateMachineStream reader) throws IOException {
        boolean result;
        do {
            StateParser state = new StateParser();
            result = state.parse(reader);
            if (result) {
                states.add(state);
                LOG.log(Level.FINEST, "parse state ok: {0}", state);
            }
            else {
                if (state.name.value.isEmpty() && reader.isEof()) {
                    // end of file; not an actual problem
                } else {
                    LOG.log(Level.WARNING, "failed to parse state: {0}", state.name.value);
                }
            }
        }
        while (result);
        // check eof
        result = reader.isEof();
        if (result) {
            LOG.log(Level.FINEST, "eof");
        }
        else {
            LOG.log(Level.WARNING, "more things in the stream");
            LOG.log(Level.WARNING, "last well parsed item was: {0}", reader.getLastSuccessful());
        }
        //
        reader.trace(this, result);
        //
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(1024);
        sb.append("StateMachine {states = ").append(states).append("}");
        return sb.toString();
    }

    /**
     * Writes spaces, at least one space, until the target column is reached.
     * <p>
     * ...
     *
     * @param w
     * @param currentColumn
     * @param targetColumn
     * @return the current column after padding
     * @throws IOException
     */
    private int padToColumn(Writer w, int currentColumn, int targetColumn) throws IOException {
        int column = currentColumn;
        do {
            w.write(' ');
            column++;
        }
        while (column < targetColumn);
        return column;
    }

    private int dump(Writer w, ArrayList<ParameterParser> list) throws IOException {
        int size = 0;
        boolean first = true;
        if (list.size() > 0) {
            w.write("(");
            size += 1;
            for (ParameterParser p : list) {
                if (first) {
                    first = false;
                }
                else {
                    w.write("; ");
                    size += 2;
                }
                w.write(p.name.value);
                size += p.name.value.length();
                if (p.value != null) {
                    w.write(" = ");
                    size += 3;
                    w.write(p.value);
                    size += p.value.length();
                }
            }
            w.write(")");
            size += 1;
        }
        return size;
    }

    private <T extends EventParser> void dumpList(Writer w, ArrayList<T> list) throws IOException {
        boolean first = true;
        w.write("{ ");
        for (T a : list) {
            if (first) {
                first = false;
            }
            else {
                w.write(", ");
            }
            w.write(a.name.value);
            dump(w, a.parameters);
        }
        w.write(" }");
    }

    /**
     * Pretty printing of a state-machine configuration.
     *
     * @param w the writer (output stream)
     * @throws IOException if something goes wrong while writing the state-machine on the stream
     */
    public void dump(Writer w) throws IOException {
        final int IDENT = 4;
        final int EA_WIDTH = 24;
        //
        for (StateParser s : states) {
            w.write(s.name.value);
            w.write(" {\n");
            if (s.isInit.get()) {
                padToColumn(w, 0, IDENT);
                w.write("init\n");
            }
            if (s.isFinal.get()) {
                padToColumn(w, 0, IDENT);
                w.write("final\n");
            }
            if (!s.onEntry.isEmpty()) {
                padToColumn(w, 0, IDENT);
                w.write("onentry   ");
                dumpList(w, s.onEntry);
                w.write("\n");
            }
            if (!s.onExit.isEmpty()) {
                padToColumn(w, 0, IDENT);
                w.write("onexit    ");
                dumpList(w, s.onExit);
                w.write("\n");
            }
            if (!s.onEvent.isEmpty()) {
                padToColumn(w, 0, IDENT);
                w.write("onevent   {");
                for (TransitionParser t : s.onEvent) {
                    int currentColumn;
                    w.write("\n");
                    currentColumn = padToColumn(w, 0, IDENT * 2);
                    w.write(t.event.name.value);
                    currentColumn += t.event.name.value.length();
                    currentColumn += dump(w, t.event.parameters);
                    currentColumn = padToColumn(w, currentColumn, IDENT * 2 + EA_WIDTH);
                    w.write(t.transitionDestination.stateName.value);
                    currentColumn += t.transitionDestination.stateName.value.length();
                    padToColumn(w, currentColumn, IDENT * 2 + EA_WIDTH * 2);
                    dumpList(w, t.transitionDestination.actionList);
                }
                w.write("\n");
                padToColumn(w, 0, IDENT);
                w.write("}\n");
            }
            if (s.timeout.get() > 0) {
                int currentColumn;
                padToColumn(w, 0, IDENT);
                w.write("ontimeout {\n");
                currentColumn = padToColumn(w, 0, IDENT * 2);
                String stimeout = s.timeout.toString();
                w.write(stimeout);
                currentColumn += stimeout.length();
                currentColumn = padToColumn(w, currentColumn, IDENT * 2 + EA_WIDTH);
                w.write(s.onTimeout.stateName.value);
                currentColumn += s.onTimeout.stateName.value.length();
                padToColumn(w, currentColumn, IDENT * 2 + EA_WIDTH * 2);
                dumpList(w, s.onTimeout.actionList);
                w.write("\n");
                padToColumn(w, 0, IDENT);
                w.write("}\n");
            }
            if (!s.ignores.isEmpty()) {
                padToColumn(w, 0, IDENT);
                w.write("ignores   ");
                dumpList(w, s.ignores);
                w.write("\n");
            }
            w.write("}\n");
        }
        w.flush();
    }

    /**
     *
     */
    public void dump() {
        try {
            dump(new OutputStreamWriter(System.out));
        }
        catch (IOException ex) {
            LOG.log(Level.WARNING, "could not dump the state-machine on stdout", ex);
        }
    }
    
}
