/*
 * Copyright (c) 2013-2014, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.statemachine.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility class to parse a state-machine definition from a file or 
 * a stream (reader).
 *
 * @author Florin Ganea
 */
public class StateMachineReader {
    
    private static final Logger LOG = Logger.getLogger(StateMachineReader.class.getPackage().getName());
    
    private final StateMachineStream reader;
    
    static boolean isSeparator(char ch) {
        return ch == ' ' || ch == '\t' || isEol(ch);
    }

    static boolean isEol(char ch) {
        return ch == '\n' || ch == '\r';
    }
    
    /**
     *
     * @param fileName
     * @throws FileNotFoundException
     */
    public StateMachineReader(String fileName) throws FileNotFoundException {
        this.reader = new StateMachineStream(fileName);
    }
    
    /**
     *
     * @param reader
     */
    public StateMachineReader(Reader reader) {
        this.reader = new StateMachineStream(reader);
    }

    /**
     *
     * @return
     */
    public StateMachineStream getStateMachineStream() {
        return this.reader;
    }
    
    /**
     *
     * @return
     * @throws ConfigException
     */
    public StateMachineParser parse() throws ConfigException {
        StateMachineParser sm = new StateMachineParser();
        boolean result;
        try {
            result = sm.parse(reader);
        }
        catch (IOException ex) {
            throw new ConfigException("I/O exception while reading the state-machine", ex);
        }
        if (result) {
            LOG.log(Level.FINEST, "success");
        } else {
            LOG.log(Level.WARNING, "failed; last parsed item: {0}", reader.getLastSuccessful());
            throw new ConfigException("could not parse correctly the state-machine from the input reader");
        }
        return sm;
    }

}
