/*
 * Copyright (c) 2013-2014, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.statemachine.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Parser for a state-machine Event.
 * <p>
 * &lt;event&gt; :== &lt;identifier&gt; | &lt;identifier&gt; '(' &lt;parameters&gt; ')'
 */
public class EventParser extends ListItemParser {
    
    private static final Logger LOG = Logger.getLogger(EventParser.class.getPackage().getName());

    /**
     *
     */
    public final IdentifierParser name;

    /**
     *
     */
    public final ArrayList<ParameterParser> parameters;

    {
        this.name = new IdentifierParser();
        this.parameters = new ArrayList<>(4);
    }

    @Override
    public boolean parse(StateMachineStream reader) throws IOException {
        boolean result;
        // read event name
        result = name.parse(reader);
        if (result) {
            // name ok
            // try to check parameters...
            reader.mark(1024);
            TokenParser t = new TokenParser();
            result = t.parse(reader);
            if (result) {
                switch (t.value) {
                    case "(":
                        // parameters...
                        while (result) {
                            ParameterParser p = new ParameterParser();
                            result = p.parse(reader);
                            if (result) {
                                this.parameters.add(p);
                                if (p.isLast.get()) {
                                    break;
                                }
                            }
                        }
                        break;
                    case ",":
                        // end of event
                        // nothing to do
                        break;
                    case "}":
                        // end of event list
                        // set last flag
                        isLast.set(true);
                        break;
                    default:
                        // something else, put it back
                        LOG.log(Level.FINEST, "something else in the token {0}, put it back", t);
                        reader.reset();
                }
            }
        }
        else {
            TokenParser t = new TokenParser();
            result = t.parse(reader);
            if (result) {
                switch (t.value) {
                    case "}":
                        isLast.set(true);
                        result = false;
                        break;
                    case ",":
                        // event follows
                        result = parse(reader);
                        break;
                    default:
                        // unexpected, put it back
                        LOG.log(Level.FINEST, "unexpected token {0}, put it back", t);
                        reader.reset();
                }
            }
        }
        //
        reader.trace(this, result);
        //
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(32);
        sb.append("Event {name = ").append(name).append(", parameters = ").append(parameters).append("}");
        return sb.toString();
    }
    
}
