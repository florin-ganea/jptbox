/*
 * Copyright (c) 2013-2014, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.statemachine;

/**
 * State-machine Action interface.
 * 
 * @author Florin Ganea
 */
public interface Action {

    /**
     *
     */
    public static enum Type {
        /** action that is executed synchronous on some external system blocking while waiting for 
         * response; however the response is provided as incoming event
         * TODO this type of actions may be executed by a separate thread? */
        SYNCHRONOUS,

        /** action that is executed asynchronously on some external system, the answer is retrieved
         * as an incoming event */
        ASYNCHRONOUS,

        /** state-machine internal action (like flag check or short calculation) that should provide 
         * the result immediately; the result is provided as internal event */
        INTERNAL
    }

    /**
     *
     * @return
     */
    public Action.Type getType();

    /**
     * Executes this Action that was in the Action list of the State...
     * 
     * @param session
     * @param state
     * @param event
     * @param parameters 
     */
    public void execute(Session session, State state, Event event, Parameters parameters);

}
