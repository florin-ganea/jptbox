/*
 * Copyright (c) 2015-2016, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.benchmark;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Benchmark utility.
 * 
 * Executes a {@link Runnable} task for (approximatively) a specified period 
 * of time and calculates the TEPS (Task Executions Per Second).
 * <p> 
 * Partial TEPS is available at any time during benchmark execution.
 * <p>
 * 
 * <p>
 * Thread safety:
 * {@link #run()} method of the same instance should not be called concurrently 
 * from multiple threads. <br>
 * The implementation is NOT 100% thread safe, but it is consistent enough for a
 * benchmark. Proper synchronization may actually impact the benchmark results
 * by measuring synchronization operations that do not belong to 
 * the benchmark-ed task.
 *
 * @author Florin Ganea
 */
public class Benchmark implements Runnable {
    
    /** 
     * Static scheduler internally used for stopping the benchmark after
     * the specified timeout. 
     * It can be used by extension classes to schedule various tasks 
     * (e.g. report / log TEPS once per second).
     */
    protected static final Timer timer;

    /** the benchmark-ed task */
    protected final Runnable task;

    /**
     *
     */
    protected final long units;

    /**
     *
     */
    protected final BenchmarkType type;

    /**
     *
     */
    protected final boolean report;
    
    /** flag used to stop the benchmark execution */
    private boolean running;
    /** flag set to true inside run method; used to prevent concurrent executions */
    private final AtomicBoolean runMethod;
    
    /** benchmark execution start time; 0L if not started */
    private long startTime;
    /** benchmark execution end time; 0L if not started or not ended */
    private long endTime;
    /** the current number of times the benchmark-ed task has been executed */
    private long counter;
    
    /** the history of benchmark runs */
    public final List<HistoryRecord> history;
    
    /**
     * Timer task used to stop the benchmark execution after the specified 
     * timeout.
     */
    private static class StopTimerTask extends TimerTask {
        
        private final Benchmark bm;
        
        protected StopTimerTask(Benchmark bm) {
            this.bm = bm;
        }
        
        @Override
        public void run() {
            bm.stop();
        }
        
    }
    
    /**
     * Data stored in a benchmark history object.
     */
    public static class HistoryRecord {

        /**
         * The benchmark start time.
         */
        public final long startTime;

        /**
         * The benchmark end time.
         */
        public final long endTime;

        /**
         * The benchmark result in TEPS.
         */
        public final double teps;
        
        /**
         * Constructor for a benchmark history object.
         * 
         * @param startTime the benchmark start time
         * @param endTime   the benchmark end time
         * @param teps      the benchmark result in TEPS
         */
        public HistoryRecord(long startTime, long endTime, double teps) {
            this.startTime = startTime;
            this.endTime = endTime;
            this.teps = teps;
        }
    }
    
    static {
        // benchmark scheduler thread as daemon
        timer = new Timer("BenchmarkScheduler", true);
    }
    
    {
        this.history = new CopyOnWriteArrayList<>();
        this.runMethod = new AtomicBoolean(false);
        this.running = false;
        this.startTime = 0L;
        this.endTime = 0L;
        this.counter = 0L;
    }
    
    /**
     * Creates a benchmark instance of the given task.
     * <p>
     * null task is illegal.
     * <p>
     * Negative or zero units values are illegal.
     * 
     * @param aTask the task that is benchmark-ed
     * @param type  the type of the benchmark
     * @param units the approximative running time in milliseconds if the type 
     *                  is {@link BenchmarkType#TIMED} or the number of 
     *                  iterations when the type is {@link BenchmarkType#ITERATED}
     * @param report whether the running method reports its progress from time
     *                  to time; useful to disable {@link BenchmarkType#ITERATED}
     *                  extra operations; not relevant for the other types
     */
    public Benchmark(Runnable aTask, BenchmarkType type, long units, boolean report) {
        if (aTask == null) {
            throw new IllegalArgumentException("null task");
        }
        
        // negative timeout is illegal
        if (units <= 0 && type != BenchmarkType.CONTINUOUS) {
            throw new IllegalArgumentException("negative or zero units values are not allowed");
        }
        
        this.task = aTask;
        this.units = units;
        this.type = type;
        this.report = report;
    }
    
    /**
     * Executes the task repeatedly until timeout (ms) expires or until the 
     * maximum number of iterations is performed.
     * <p>
     * Attempting to execute this method concurrently results in 
     * {@link IllegalStateException}.
     * 
     * @throws IllegalStateException this method is attempted to be run concurrently
     */
    @Override
    public void run() {
        // running concurrently this method is illegal
        // mark that the run method has been started
        if (!this.runMethod.compareAndSet(false, true)) {
            throw new IllegalStateException("concurrent execution of the benchmark is not allowed");
        }
        
        TimerTask tt = null;
        
        if (type == BenchmarkType.TIMED) {
            tt = new StopTimerTask(this);
            Benchmark.timer.schedule(tt, units);
        }
        
        long lcounter;
        long lstart;
        long lend;
        boolean lrunning;
        long checkpoint = 0L;
        long lnext = 0L;

        synchronized (this) {
            lstart = this.startTime = System.currentTimeMillis();
            this.endTime = 0L;
            lcounter = this.counter = 0L;
            lrunning = this.running = true;
        }
        
        // execution loop
        while (lrunning && 
                (type != BenchmarkType.ITERATED || lcounter < units)) {
            // execute task
            this.task.run();
            lcounter++;

            if (report || type != BenchmarkType.ITERATED) {
                // hi-speed optimization -- determine checkpoint
                if (lcounter == 1_000L) {
                    long lnowdiff = System.currentTimeMillis() - lstart;
                    if (lnowdiff == 0L) { // faster than 1000/ms
                        checkpoint = 1_000_000L;
                    }
                    else
                    if (lnowdiff < 10L) { // faster than 100/ms
                        checkpoint = 100_000L;
                    }
                    else 
                    if (lnowdiff < 100L) { // faster than 10/ms
                        checkpoint = 10_000L;
                    }
                    else 
                    if (lnowdiff < 1_000L) { // faster than 1/ms
                        checkpoint = 1_000L;
                    }
                    // else it is irrelevant ...
                }

                // either regular step or hi-speed optimization checkpoint
                if (lcounter >= lnext) {
                    synchronized (this) {
                        lrunning = this.running;
                        this.counter = lcounter;
                    }
                    if (checkpoint > 0L) {
                        lnext += checkpoint;
                    }
                }
            }
        }
        
        // execution finished
        synchronized (this) {
            lend = this.endTime = System.currentTimeMillis();
            this.counter = lcounter;
            this.running = false;
        }
        
        // cancel the timer task in order to avoid accidental execution
        if (tt != null) {
            tt.cancel();
        }
        
        this.history.add(new HistoryRecord(lstart, lend, getTEPS()));
        
        // mark that the run method has exited
        this.runMethod.set(false);
    }
    
    /**
     * Stops the running benchmark after finishing the current task execution.
     * <p>
     * This method can be called at any time during benchmark execution.
     * <p>
     * This method is also called by the internal timer task used to stop the
     * benchmark after the specified timeout.
     */
    public void stop() {
        synchronized (this) {
            this.running = false;
        }
    }
    
    /**
     *
     * @return
     */
    public BenchmarkStatus getStatus() {
        synchronized (this) {
            return new BenchmarkStatus(startTime, endTime, counter, running);
        }
    }
    
    /**
     * Gets the current TEPS (Task Executions Per Second).
     * <p>
     * It can be called at the end of the benchmark (e.g. after {@link #run(long)}
     * method finishes) or at any time during benchmark execution.
     * 
     * @return the current TEPS
     */
    public double getTEPS() {
        return getTEPS(this.getStatus());
    }
    
    /**
     *
     * @return
     */
    public double getExecutionTime() {
        return getExecutionTime(this.getStatus());
    }
    
    /**
     *
     * @param status
     * @return
     */
    public static double getExecutionTime(BenchmarkStatus status) {
        double executionTime = status.endTime - status.startTime;
        
        if (executionTime < 0L) {
            // benchmark not finished; use the current time as the end time
            executionTime += System.currentTimeMillis();
        }
        
        executionTime /= 1000.0; // seconds
        
        return executionTime;
    }
    
    /**
     *
     * @param status
     * @return
     */
    public static double getTEPS(BenchmarkStatus status) {
        double teps;
        double executionTime = getExecutionTime(status);
        
        if (executionTime > 0L) {
            teps = (double) status.counter / executionTime;
        } else {
            // irrevevant execution time
            teps = 0.0;
        }
        
        return teps;
    }
    
    /**
     * Utility method to create a benchmark, run it and provide the result as 
     * return value.
     * 
     * @param task  the task to be benchmark-ed
     * @param type  the type of the benchmark
     * @param units the approximative running time in milliseconds if the type 
     *                  is {@link BenchmarkType#TIMED} or the number of 
     *                  iterations when the type is {@link BenchmarkType#ITERATED}
     * 
     * @return the benchmark TEPS (Task Executions Per Second) after execution
     */
    public static double benchmark(Runnable task, BenchmarkType type, long units) {
        Benchmark bm = new Benchmark(task, type, units, false);
        bm.run();
        return bm.getTEPS();
    }
    
    /**
     *
     * @param tasks
     * @param type
     * @param units
     * @return
     */
    public static BenchmarkStatus[] benchmarkMT(Runnable[] tasks, BenchmarkType type, long units) {
        int threadCount = tasks.length;
        BenchmarkStatus[] ateps = new BenchmarkStatus[threadCount];
        Thread[] athread = new Thread[threadCount];
        Benchmark[] abm = new Benchmark[threadCount];
        
        for (int i = 0; i < threadCount; i++) {
            abm[i] = new Benchmark(tasks[i], type, units, false);
            athread[i] = new Thread(abm[i]);
        }
        
        for (int i = 0; i < threadCount; i++) {
            athread[i].start();
        }

        for (int i = 0; i < threadCount; i++) {
            try {
                athread[i].join();
                ateps[i] = abm[i].getStatus();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
                // NOOP
            }
        }
        
        return ateps;        
    }

}
