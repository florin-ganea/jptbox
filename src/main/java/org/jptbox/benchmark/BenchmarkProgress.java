/*
 * Copyright (c) 2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.jptbox.benchmark;

import org.jptbox.progress.Bouncer;
import org.jptbox.progress.Fixed;
import org.jptbox.progress.Percentage;
import org.jptbox.progress.Progress;
import org.jptbox.progress.ProgressBar;
import org.jptbox.progress.spi.ProgressInfo;
import org.jptbox.progress.spi.ProgressInfoIm;
import org.jptbox.progress.spi.ProgressProvider;
import org.jptbox.progress.RollingChar;

/**
 * Benchmark utility accompanied by a progress bar.
 * 
 * @author Florin Ganea
 */
public class BenchmarkProgress extends Benchmark implements ProgressProvider {

    /**
     *
     * @param aTask
     * @param type
     * @param units
     */
    public BenchmarkProgress(Runnable aTask, BenchmarkType type, long units) {
        super(aTask, type, units, true);
    }

    /**
     *
     * @param title
     * @param task
     * @param type
     * @param units
     * @param progressWidth
     * @return
     */
    public static double benchmark(String title,  
            Runnable task, BenchmarkType type, 
            long units, int progressWidth) {
        
        // progress chars
        char empty = '.';
        char fill = '=';
        
        BenchmarkProgress bm = new BenchmarkProgress(task, type, units);

        ProgressBar pb = new ProgressBar(bm, progressWidth);
        pb.add(new Fixed(pb, title));
        
        if (type == BenchmarkType.CONTINUOUS) {
            pb.add(new Fixed(pb, "["));
            pb.add(new Bouncer(pb, empty, fill, 0.4));
            pb.add(new Fixed(pb, "]"));
        } else {
            pb.add(new Fixed(pb, "["));
            pb.add(new Progress(pb, empty, fill));
            pb.add(new Fixed(pb, "]"));
            pb.add(new RollingChar(pb));
            pb.add(new Percentage(pb));
        }

        pb.initialize();
        
        bm.run();
        
        pb.terminate(true);
        
        return bm.getTEPS();
    }

    /**
     *
     * @return
     */
    @Override
    public ProgressInfo getProgressInfo() {
        ProgressInfo info;
        long currentCount;
        long targetCount;
        
        BenchmarkStatus status = this.getStatus();
        
        if (this.type == BenchmarkType.ITERATED) {        
            currentCount = status.counter;
        } else {
            currentCount = System.currentTimeMillis() - status.startTime;
        }
        
        targetCount = this.units < currentCount ? currentCount : this.units;
        info = new ProgressInfoIm(currentCount, targetCount, status.startTime, status.endTime);
        
        return info;
    }

}
