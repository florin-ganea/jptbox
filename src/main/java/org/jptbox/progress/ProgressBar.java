/*
 * Copyright (c) 2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jptbox.progress;

import org.jptbox.progress.spi.ProgressInfo;
import org.jptbox.progress.spi.ProgressProvider;

/**
 * Progress bar as a container of progress elements with automatic update inner 
 * thread.
 * 
 * Thread-safety: 
 * - The progress bar has an own rendering and painting thread. All intermediary
 * updates are performed by this thread at regular intervals.
 * - The rendering and painting thread gets progress information from the 
 * progress provider. The progress information instances have to be either 
 * thread-safe or immutable.
 * 
 * @author Florin Ganea
 */
public class ProgressBar extends ProgressElementContainer {

    /**
     *
     */
    public static final int DEFAULT_WIDTH = 80;

    /**
     *
     */
    public static final long DEFAULT_REFRESH_INTERVAL = 150L;

    /**
     *
     */
    public static final long DEFAULT_TARGET_VALUE = 100L; // 100%

    static class ProgressBarUpdateThread extends Thread {
        
        private final ProgressBar bar;
        private final ProgressProvider provider;
        private final long refreshInterval;

        private final Object monitor;
        private boolean running;
        
        {
            this.monitor = new Object();
            this.running = true;
        }

        public ProgressBarUpdateThread(ProgressBar bar, 
                ProgressProvider provider, long refreshInterval) {
            super("ProgressBarUpdateThread");
            this.setDaemon(true);
            this.bar = bar;
            this.provider = provider;
            this.refreshInterval = refreshInterval;
        }
        
        @Override
        public void run() {
            final long sleepInterval = this.refreshInterval;

            // fixed rate wakeups
            long currentTime = System.currentTimeMillis();
            long wakeupTime = currentTime;

            // using a monitor to block thread termination 
            // while rendering / painting
            synchronized (this.monitor) {
                while (this.running) {
                    ProgressInfo info = this.provider.getProgressInfo();
                    if (this.bar.render(info)) {
                        this.bar.getDisplay().paint();
                    }
                    
                    // stop the thread if the task has finished
                    if (info.getEndTime() > 0) {
                        this.running = false;
                    }
                    
                    // wait until next theoretical wakeup time
                    wakeupTime += sleepInterval;
                    currentTime = System.currentTimeMillis();
                    while (this.running && wakeupTime > currentTime) {
                        try {
                            this.monitor.wait(wakeupTime - currentTime);
                        } catch (InterruptedException ex) {
                            // NOOP
                        }
                        currentTime = System.currentTimeMillis();
                    }
                }
            }
        }
        
        public void terminate(boolean graceful) {
            // set the running flag to false and wakeup the thread
            synchronized (this.monitor) {
                this.running = this.running && graceful;
                this.monitor.notifyAll();
            }
        }
        
    }
    
    private final ProgressBarUpdateThread updateThread;

    /**
     *
     */
    protected final ProgressProvider provider;
    
    /**
     *
     * @param provider
     * @param width
     * @param refreshInterval
     */
    public ProgressBar(ProgressProvider provider, int width, long refreshInterval) {
        super(null, width);
        this.provider = provider;
        this.updateThread = new ProgressBarUpdateThread(this, this.provider, refreshInterval);
    }
    
    /**
     *
     * @param provider
     * @param width
     */
    public ProgressBar(ProgressProvider provider, int width) {
        this(provider, width, DEFAULT_REFRESH_INTERVAL);
    }
    
    /**
     *
     * @param provider
     */
    public ProgressBar(ProgressProvider provider) {
        this(provider, DEFAULT_WIDTH, DEFAULT_REFRESH_INTERVAL);
    }
    
    /**
     *
     */
    public void initialize() {
        // trigger layout of all components tree
        this.setLayout(0);
        // start the painting thread
        this.updateThread.start();
    }
    
    /**
     *
     * @param graceful
     */
    public void terminate(boolean graceful) {
        this.updateThread.terminate(graceful);
        try {
            // wait 1s
            this.updateThread.join(1000L);
            // still alive?
            if (this.updateThread.isAlive()) {
                // forced termination
                this.updateThread.terminate(false);
                // wait another 1s
                this.updateThread.join(1000L);
            }
        } catch (InterruptedException ex) {
            // NOOP
        }
    }

}
