/*
 * Copyright (c) 2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jptbox.progress;

import java.util.Arrays;

/**
 *
 * @author Florin Ganea
 */
public class Bouncer extends Progress {
    
    private char[] roll;
    private int rollSeq;
    private final double fillPercentage;
    
    /**
     *
     * @param parent
     * @param empty
     * @param fill
     * @param fillPercentage
     */
    public Bouncer(ProgressElement parent, char empty, char fill, double fillPercentage) {
        super(parent, empty, fill);
        
        this.roll = null;
        this.rollSeq = 0;
        
        if (fillPercentage <= 0.0 || fillPercentage >= 1.0) {
            throw new IllegalArgumentException("invalid fill percentage; allowed only (0..1)");
        }        
        this.fillPercentage = fillPercentage;
    }

    /**
     *
     * @param parent
     * @param roll
     * @param empty
     * @param fill
     */
    public Bouncer(ProgressElement parent, String roll, char empty, char fill) {
        super(parent, empty, fill);
        this.roll = roll.toCharArray();
        this.rollSeq = 0;
        this.fillPercentage = 0.0;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean renderInitial() {
        boolean changed = false;
        for (int i = 0; i < this.getWidth(); i++) {
            changed = this.setIfDifferent(i, this.empty) || changed;
        }
        return changed;
    }
    
    /**
     *
     * @return
     */
    @Override
    public boolean renderFinal() {
        boolean changed = false;
        for (int i = 0; i < this.getWidth(); i++) {
            changed = this.setIfDifferent(i, this.fill) || changed;
        }
        return changed;
    }

    /**
     *
     * @param percentage
     * @return
     */
    @Override
    public boolean render(double percentage) {
        boolean changed = false;
        
        if (fillPercentage > 0) {
            int expectedLength = (int) (this.getWidth() * fillPercentage);
            if (this.roll == null || this.roll.length != expectedLength) {
                this.roll = new char[expectedLength];
                Arrays.fill(this.roll, this.fill);
            }
        }
        
        // for the text to be painted fully, it can start at any index starting
        // with 0, but no more than (element width - text length)
        
        // bouncing algorithm:
        // (1) start painting on index 0
        // (2) increment painting index until max start index
        // (3) decrement painting index until 0
        // (4) continue with (2)
        
        // the period of this algorithm is 2 * max start index
        
        int wid = this.getWidth();
        int max = wid - roll.length; // max start index
        int istart = rollSeq;
        
        if (rollSeq > max) {
            istart = (max << 1) - rollSeq; // 2max - seq
        } 
        
        rollSeq++;
        if (rollSeq >= (max << 1)) { // seq >= 2max
            rollSeq = 0;
        }
        
        for (int i = 0; i < istart; i++) {
            changed = this.setIfDifferent(i, empty) || changed;
        }
        for (int i = istart; i < roll.length + istart; i++) {
            changed = this.setIfDifferent(i, roll[i - istart]) || changed;
        }
        for (int i = roll.length + istart; i < wid; i++) {
            changed = this.setIfDifferent(i, empty) || changed;
        }

        return changed;
    }    
    
}
