/*
 * Copyright (c) 2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jptbox.progress;

/**
 *
 * @author Florin Ganea
 */
public class DisplayLine implements Display {
    
    private final char[] line;
    
    /**
     *
     * @param width
     */
    public DisplayLine(int width) {
        this.line = new char[width];
        // fill some garbage
        for (int i = 0; i < this.line.length; i++) {
            this.line[i] = '_';
        }
    }

    /**
     *
     * @param index
     * @param value
     * @return
     */
    @Override
    public boolean setIfDifferent(int index, char value) {
        boolean change = line[index] != value;
        if (change) {
            line[index] = value;
        }
        return change;        
    }

    /**
     *
     * @param index
     * @param value
     */
    @Override
    public void set(int index, char value) {
        line[index] = value;
    }
    
    /**
     *
     */
    @Override
    public void paint() {
        System.out.print('\r');
        System.out.print(String.valueOf(this.line));
    }    

}
