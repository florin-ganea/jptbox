/*
 * Copyright (c) 2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jptbox.progress;

import java.util.ArrayList;
import java.util.List;
import org.jptbox.progress.spi.ProgressInfo;

/**
 *
 * @author Florin Ganea
 */
public class ProgressElementContainer extends ProgressElement {
    
    private final List<ProgressElement> children;

    {
        this.children = new ArrayList<>();
    }

    /**
     *
     * @param parent
     * @param width
     */
    public ProgressElementContainer(ProgressElement parent, int width) {
        super(parent, width);
    }

    /**
     *
     * @param element
     */
    public void add(ProgressElement element) {
        this.children.add(element);
    }
    
    /**
     * Arrange visual items using given order and width types.
     * Dynamic types will be evenly distributed.
     */
    private void layout(int offset) {
        int fw = 0; // width consumed by fixed-width items
        int dc = 0; // dynamic-width count
        for (ProgressElement elem : this.children) {
            if (elem.getWidth() > 0) {
                fw += elem.getWidth();
            } else {
                dc++;
            }
        }
        // remaining
        int dw = (this.getWidth() - fw) / dc; // dynamic-width split
        int dwr = (this.getWidth() - fw) % dc; // remainder; distributed to first items
        int co = offset;
        for (ProgressElement elem : this.children) {
            if (elem.getWidth() <= 0) {
                if (dwr > 0) {
                    elem.setLayout(co, dw + 1);
                    co += dw + 1;
                    dwr--;
                } else {
                    elem.setLayout(co, dw);
                    co += dw;
                }
            } else {
                elem.setLayout(co);
                co += elem.getWidth();
            }
        }
    }

    /**
     *
     * @param offset
     */
    @Override
    protected void setLayout(int offset) {
        super.setLayout(offset);
        this.layout(offset);
    }

    /**
     *
     * @param offset
     * @param width
     */
    @Override
    protected void setLayout(int offset, int width) {
        super.setLayout(offset, width);
        this.layout(offset);
    }

    /**
     *
     * @return
     */
    @Override
    public boolean renderInitial() {
        boolean changed = false;
        for (ProgressElement elem : children) {
            changed = elem.renderInitial() || changed;
        }
        return changed;
    }
    
    /**
     *
     * @return
     */
    @Override
    public boolean renderFinal() {
        boolean changed = false;
        for (ProgressElement elem : children) {
            changed = elem.renderFinal() || changed;
        }
        return changed;
    }

    /**
     *
     * @param percentage
     * @return
     */
    @Override
    public boolean render(double percentage) {
        boolean changed = false;
        for (ProgressElement elem : children) {
            changed = elem.render(percentage) || changed;
        }
        return changed;
    }
    
    /**
     *
     * @param info
     * @return
     */
    @Override
    public boolean render(ProgressInfo info) {
        boolean changed = false;
        for (ProgressElement elem : children) {
            changed = elem.render(info) || changed;
        }
        return changed;
    }

}
