/*
 * Copyright (c) 2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jptbox.progress;

import java.util.Objects;
import org.jptbox.progress.spi.ProgressInfo;

/**
 *
 * @author Florin Ganea
 */
public abstract class ProgressElement {
    
    private final ProgressElement parent;
    private final Display display;

    private int offset;
    private int width;
    
    {
        this.offset = 0;
    }

    /**
     *
     * @param parent
     */
    protected ProgressElement(ProgressElement parent) {        
        this.parent = Objects.requireNonNull(
                parent, 
                "parent must not be null when width is not provided");
        this.display = null;
        this.width = -1;
    }

    /**
     *
     * @param parent
     * @param width
     */
    protected ProgressElement(ProgressElement parent, int width) {
        this.parent = parent;
        this.width = width;
        if (this.parent == null) {
            this.display = new DisplayLine(this.width);
        } else {
            this.display = null;
        }
    }

    /**
     *
     * @return
     */
    public Display getDisplay() {
        return this.parent != null ? this.parent.getDisplay() : this.display;
    }

    /**
     *
     * @return
     */
    public int getWidth() {
        return this.width;
    }

    /**
     *
     * @param offset
     * @param width
     */
    protected void setLayout(int offset, int width) {
        this.offset = offset;
        this.width = width;
    }

    /**
     *
     * @param offset
     */
    protected void setLayout(int offset) {
        this.offset = offset;
    }

    /**
     *
     * @param index
     * @param value
     * @return
     */
    protected final boolean setIfDifferent(int index, char value) {
        return this.getDisplay().setIfDifferent(offset + index, value);
    }
    
    /**
     *
     * @param value
     * @return
     */
    protected final boolean setIfDifferent(String value) {
        boolean changed = false;
        
        for (int i = 0; i < value.length(); i++) {
            changed = setIfDifferent(i, value.charAt(i)) || changed;
        }
        
        return changed;
    }
    
    /**
     *
     * @return
     */
    public boolean renderInitial() {
        return render(0.0);
    }
    
    /**
     *
     * @return
     */
    public boolean renderFinal() {
        return render(1.0);
    }

    // this is render, not actual paint

    /**
     *
     * @param percentage
     * @return
     */
    public abstract boolean render(double percentage);
    
    /**
     *
     * @param info
     * @return
     */
    public boolean render(ProgressInfo info) {
        boolean changed;
        
        if (info.getStartTime() == 0L) {
            // progress not started
            changed = this.renderInitial();
        } else if (info.getEndTime() > 0L) {
            // progress started and already ended
            changed = this.renderFinal();
        } else {
            // started, but not ended
            double percentage;                        
            double target = info.getTarget();
            if (target < 0) {
                // estimated target
                target = -target;
            }
            if (target > 0) {
                // estimated or known target
                percentage = info.getCurrent() / target;
            } else {
                // unknown target
                // TODO this may not be supported yet
                percentage = Double.NaN;
            }
            changed = this.render(percentage);
        }
        
        return changed;
    }

}
