/*
 * Copyright (c) 2015, Florin Ganea
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jptbox.progress.spi;

import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author Florin Ganea
 */
public class ProgressHelper implements ProgressProvider, ProgressInfo {
    
    /**
     *
     */
    protected long startTime;

    /**
     *
     */
    protected long endTime;

    /**
     *
     */
    protected long targetCount;

    /**
     *
     */
    protected final AtomicLong currentCount;

    {
        this.startTime = 0L;
        this.endTime = 0L;
        this.currentCount = new AtomicLong(0L);
    }
    
    /**
     *
     * @param targetCount
     */
    public ProgressHelper(long targetCount) {
        this.targetCount = targetCount;
    }
    
    /**
     *
     * @return
     */
    @Override
    public ProgressInfo getProgressInfo() {
        return this;
    }

    @Override
    public long getStartTime() {
        synchronized (this) {
            return this.startTime;
        }
    }

    @Override
    public long getEndTime() {
        synchronized (this) {
            return this.endTime;
        }
    }

    @Override
    public String getUnits(long count) {
        return "";
    }

    @Override
    public long getTarget() {
        synchronized (this) {
            return this.targetCount;
        }
    }

    @Override
    public long getCurrent() {
        return this.currentCount.get();
    }

    @Override
    public String getCurrentLabel() {
        return "";
    }

    /**
     *
     * @param current
     * @param target
     */
    public void setCurrentCount(long current, long target) {
        synchronized (this) {
            this.currentCount.set(current);
            this.targetCount = target;
        }
    }

    /**
     *
     * @param current
     */
    public void setCurrentCount(long current) {
        this.currentCount.set(current);
    }

    /**
     *
     */
    public void progress() {
        this.currentCount.incrementAndGet();
    }

    /**
     *
     */
    public void progressStarted() {
        synchronized (this) {
            this.startTime = System.currentTimeMillis();
        }
    }

    /**
     *
     */
    public void progressEnded() {
        synchronized (this) {
            this.endTime = System.currentTimeMillis();
        }
    }
    
}
