/*
 * Copyright (c) 2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jptbox.progress.spi;

/**
 *
 * @author Florin Ganea
 */
public class ProgressInfoIm implements ProgressInfo {
    
    private final long startTime;
    private final long endTime;
    private final long currentCount;
    private final long targetCount;

    /**
     *
     * @param currentCount
     * @param targetCount
     * @param startTime
     * @param endTime
     */
    public ProgressInfoIm(long currentCount, long targetCount, long startTime, long endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.currentCount = currentCount;
        this.targetCount = targetCount;
    }

    @Override
    public long getStartTime() {
        return this.startTime;
    }

    @Override
    public long getEndTime() {
        return this.endTime;
    }

    @Override
    public String getUnits(long count) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public long getTarget() {
        return this.targetCount;
    }

    @Override
    public long getCurrent() {
        return this.currentCount;
    }

    @Override
    public String getCurrentLabel() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
