/*
 * Copyright (c) 2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jptbox.progress.spi;

/**
 * An executing task whose progress needs to be displayed, exposes this interface
 * in order to provide the necessary information to the progress bar or whatever
 * displayer is used.
 * <p>
 * The progress information should be provided as an imutable object. Thread-safe
 * is not enough since there may be correlations being made between the information
 * and this may lead to inconsistency and, in the end, wrong display. However, it
 * is user decision how to implement this object.
 * <p>
 * There are some types of information that are, say, static, like the starting
 * timestamp of the task, etc. 
 * <p>
 * There are other types of information that can be calculated by the monitoring
 * process, therefore not needed to be exposed: estimated finish time, completion
 * percentage, etc. The progress information should keep at minimum the exposed
 * information.
 * <p>
 * The progress information of a task may consist in the following types of
 * information:
 * <ul>
 * <li> x (current counter) of known y (target counter) </li>
 * <li> x of estimated y </li>
 * <li> x of unknown y </li>
 * <li> x (current size) of (known, estimated, unknown) y (target size); in bytes
 * <li> current label (e.g. a file name), x of (known, estimated, unknown) y </li>
 * 
 * </ul>
 * <p>
 * Note: Known and estimated types of target may not make any difference on the 
 * displayed information.
 * <p>
 * Note: Counters, in particular targets, are positive long integers, including 0.
 * While for the current counter 0 might just a value, it is not precisely correct
 * to use 0 as a known target counter. Therefore we may have the following 
 * convention: 0 means unknown target, negative long integer means estimated 
 * target with sign changed, positive long integer means known target.
 * <p>
 * Note: It is recommended to use as counters the smallest reasonable unit for the
 * quantity they measure.
 *
 * @author Florin Ganea
 */
public interface ProgressInfo {
    
    /**
     * Gets the starting time of the task being monitored in milliseconds since
     * Epoch.
     * <p>
     * 0 (zero) is returned if the task being monitored is not started.
     * 
     * @return the starting time in milliseconds or 0 (zero) if not started
     */
    public long getStartTime();

    /**
     * Gets the ending time of the task being monitored in milliseconds since
     * Epoch.
     * <p>
     * 0 (zero) is returned if the task being monitored is not ended.
     * 
     * @return the ending time in milliseconds or 0 (zero) if not ended
     */
    public long getEndTime();
    
    /**
     * The expression of the provided count in units of measurement.
     * <p>
     * The count is a long integer representation of the reasonable smallest  
     * measured units:
     * <ul>
     * <li> milliseconds for time </li>
     * <li> bytes for size </li>
     * <li> etc. </li>
     * </ul>
     * <p>
     * For example, a task working with "MB" as measurement unit would transform
     * 1,000,000 count to "1 MB".
     * <p>
     * Note: Since the implementor of this interface provides both the 
     * counters and their representation in units of measurement, the word
     * "reasonable" above offers a great flexibility of what exactly is the 
     * meaning of counters (current or targets) in terms of units.
     * 
     * @param count the long integer representation of the reasonable smallest
     *                  measured units, as provided by the current or target 
     *                  counters
     * 
     * @return a string representation of the count in the task units of measurement
     */
    public String getUnits(long count);
    
    /**
     * Returns the target count (size, etc.) of the task progress.
     * <p>
     * A positive number returned means a known target.
     * <p>
     * A negative number returned means an estimated target with sign changed.
     * <p>
     * A value of 0 (zero) returned means an unknown target.
     * <p>
     * When a target count is not unknown the current count must be lower or at
     * most equal to it.
     * 
     * @return the target count of the task progress
     */
    public long getTarget();
    
    /**
     * Returns the current count (size, etc.) of the task progress.
     * <p>
     * It has to be either a positive number or 0 (zero) in case the task did
     * not have any progress since startup.
     * <p>
     * The current count must have values lower or at most equal to the target 
     * count, known or estimated.
     * 
     * @return 
     */
    public long getCurrent();
    
    /**
     * There are cases when the current count may be accompanied with some
     * additional information, e.g. a file name in case of a moving files task.
     * <p>
     * This method returns the additional information that may be linked to the
     * current count.
     * <p>
     * The returned value may be null or empty string in case there is no 
     * relevant information linked to the current count.
     * 
     * @return the additional information that may be linked to the current count
     */
    public String getCurrentLabel();
    
}
