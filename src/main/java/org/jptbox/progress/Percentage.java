 /*
 * Copyright (c) 2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jptbox.progress;

/**
 * ##0% -- '  2%', ' 15%', '100%'
 * 
 * @author Florin Ganea
 */
public class Percentage extends ProgressElement {
    
    /**
     *
     * @param parent
     */
    public Percentage(ProgressElement parent) {
        super(parent, 4);
    }
    
    /**
     *
     * @param percentage
     * @return
     */
    @Override
    public boolean render(double percentage) {
        boolean changed;
        
        int perc = (int) (percentage * 100.0 + 0.5);
        
        // hundreds, tens, digits
        int h = perc / 100 % 10;
        int t = perc % 100 / 10;
        int d = perc % 10;

        // hundreds significant?
        changed = setIfDifferent(0, (h == 0) ? ' ' : (char) ('0' + h));

        // tens singificant or hundreds already significant?
        changed = setIfDifferent(1, (t == 0 && h == 0) ? ' ' : (char) ('0' + t)) || changed;

        // digits are always significant
        changed = setIfDifferent(2, (char) ('0' + d)) || changed;

        changed = setIfDifferent(3, '%') || changed;            
        
        return changed;
    }
    
    /**
     *
     * @return
     */
    @Override
    public boolean renderInitial() {
        return setIfDifferent("  0%");
    }

    /**
     *
     * @return
     */
    @Override
    public boolean renderFinal() {
        return setIfDifferent("100%");
    }

}
