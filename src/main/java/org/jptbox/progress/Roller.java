/*
 * Copyright (c) 2015, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jptbox.progress;

/**
 *
 * @author Florin Ganea
 */
public class Roller extends ProgressElement {
    
    private final char initialFill;
    private final char finalFill;
    private final boolean leftToRight;
    private final char[] roll;
    private int rollSeq;
    
    /**
     *
     * @param parent
     * @param roll
     * @param initialFill
     * @param finalFill
     * @param leftToRight
     */
    public Roller(ProgressElement parent, String roll, 
            char initialFill, char finalFill, boolean leftToRight) {
        super(parent, roll.length());
        this.initialFill = initialFill;
        this.finalFill = finalFill;
        this.leftToRight = leftToRight;
        this.roll = roll.toCharArray();
        this.rollSeq = 0;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean renderInitial() {
        boolean changed = false;
        for (int i = 0; i < this.getWidth(); i++) {
            changed = this.setIfDifferent(i, this.initialFill) || changed;
        }
        return changed;
    }
    
    /**
     *
     * @return
     */
    @Override
    public boolean renderFinal() {
        boolean changed = false;
        for (int i = 0; i < this.getWidth(); i++) {
            changed = this.setIfDifferent(i, this.finalFill) || changed;
        }
        return changed;
    }

    /**
     *
     * @param percentage
     * @return
     */
    @Override
    public boolean render(double percentage) {
        boolean changed = false;
        int w = this.getWidth();
        int id, ir;
        for (int i = 0; i < w; i++) {
            if (leftToRight) {
                id = (i + rollSeq) % w;
                ir = i;
            } else {
                id = i;                
                ir = (i + rollSeq) % w;
            }
            changed = this.setIfDifferent(id, roll[ir]) || changed;
        }
        rollSeq++;
        return changed;
    }    
    
}
