/*
 * Copyright (c) 2014, Florin Ganea
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 */

grammar StateMachine;

// <eol> :== '\n' | '\r' | '\n' '\r' | '\r' '\n'
fragment EOL : '\r'? '\n' ;
// <space> :== ' ' | '\t' 
fragment SPACE : ' ' | '\t' ;
// <white-space> :== <space> | <eol>
WHITE_SPACE : (SPACE | EOL) -> skip ;

// <letter> :==  'a' - 'z' | 'A' - 'Z'
fragment LETTER : 'a' .. 'z' | 'A' .. 'Z' ;
// <digit> :== '0' - '9'
fragment DIGIT : '0' .. '9' ;
// <hex-digit> :== <digit> | 'A' - 'F' | 'a' - 'f'
fragment HEX_DIGIT : DIGIT | 'A' .. 'F' | 'a' .. 'f' ;
// <other-printable> :== '~' | '`' | '!' | '@' | ',' | ...
fragment OTHER_PRINTABLE : '!' .. '/' | ':' .. '~' ;

// <char> :== <letter> | <digit> | <space> | <other-printable>
// fragment CHAR : LETTER | DIGIT | SPACE | OTHER_PRINTABLE ;
// optimization
fragment CHAR : SPACE | '!' .. '~' ;
// <text> :== (<char>)+
fragment TEXT : CHAR* ;
// <escaped-text> :== (<char> | '\' '"')+
fragment ESCAPED_TEXT : ('\\' '"' | CHAR)* ;

// <comment> :== '#' (<text>)? <eol>
COMMENT : '#' TEXT EOL -> skip ;

// <white-spaces> :== (<white-space>)+
// <separator> :== <white-spaces> | <comment>

// basically, separators can be between tokens below; they are not put explicitly
// <separators> :== (<separator>)+

// <identifier> :== <letter> (<letter> | <digit> | '_')*
IDENTIFIER : LETTER (LETTER | DIGIT | '_')* ;

// <value-boolean> :== true | false
VALUE_BOOLEAN : 'true' | 'false' ;
// <value-numeric> :== ('-')? (<digit>)+ ('.' (<digit>)+)? | "0x" (<hex-digit>)+
VALUE_NUMERIC : '-'? DIGIT+ ('.' DIGIT+)? | '0x' HEX_DIGIT+ ;
// <value-string>  :== '"' (<escaped-text>)? '"'
// VALUE_STRING : '"' ('\\' '"' | ~[\r\n\t"])*  '"' ;
VALUE_STRING : '"' ESCAPED_TEXT '"' ;
// <value> :== <value-boolean> | <value-string> | <value-numeric> | <identifier>
value : VALUE_BOOLEAN | VALUE_STRING | VALUE_NUMERIC | IDENTIFIER ;

// state-machine is described by one or more states
// <state-machine> :== (<state>)+
stateMachine : state+ ;

// state has a name and a definition body
// <state>      :== <state-name> '{' <state-body> '}'
state : stateName '{' stateBody '}' ;
// <state-name> :== <identifier>
stateName : IDENTIFIER ;

// state definition body has one or more parameters
// <state-body>      :== (<state-parameter>)+
stateBody : stateParameter+ ;
/*
<state-parameter> :== 
        "init"  | 
        "final" | 
        "onentry"   '{' <action-list> '}' | 
        "onexit"    '{' <action-list> '}' | 
        "onevent"   '{' <transition-list> '}' | 
        "ontimeout" '{' <timeout> <transition-destination> '}' | 
        "ignores"   '{' <event-list> '}'
*/
stateParameter :
        'init'                      # Init
      | 'final'                     # Final
      | 'onentry'   actionList      # OnEntry
      | 'onexit'    actionList      # OnExit
      | 'onevent'   transitionList  # OnEvent
      | 'ontimeout' '{' timeout transitionDestination '}' # OnTimeout
      | 'ignores'   eventList       # Ignores 
;

// action list may be empty or a comma separated list of actions
// <action-list> :== (<action> (',' <action>)*)?
actionList : '{' (action (',' action)*)? '}' ;
// <event-list>  :== (<event> (',' <event>)*)?
eventList : '{' (event (',' event)*)? '}' ;

// action may have parameters
// <action> :== <identifier> | <identifier> '(' <parameters> ')'
action : IDENTIFIER | IDENTIFIER '(' parameters ')' ;
// <event>  :== <identifier> | <identifier> '(' <parameters> ')'
event : IDENTIFIER | IDENTIFIER '(' parameters ')' ;

// <transition-list> :== (<transition>)*
transitionList : '{' transition* '}' ;
// <transition>      :== <event> <transition-destination>
transition : event transitionDestination ;
// <transition-destination> :== <state-name> '{' <action-list> '}'
transitionDestination : stateName actionList ;

// <timeout> :== <value-numeric>
timeout : VALUE_NUMERIC ;

// <parameter>  :== <identifier> ('=' <value>)?
parameter : IDENTIFIER ('=' value)? ;
// <parameters> :== <parameter> (';' <parameter>)*
parameters : parameter (';' parameter)* ;
